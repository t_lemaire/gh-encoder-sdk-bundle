<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Jobs\Video;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\VideoEncodeJob;

class VideoEncodeJobTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingDownloadVideoUrlValue()
	{
		$url = 'http://www.xxx.com/xxx.mp4';

		$job = new VideoEncodeJob();

		$this->assertEquals(null, $job->getDownloadVideoUrl());

		$job->setDownloadVideoUrl($url);
		$this->assertEquals($url, $job->getDownloadVideoUrl());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Invalid download video url format.
	 */
	public function testSettingDownloadVideoUrlValueException()
	{
		$job = new VideoEncodeJob();

		try {
			$job->setDownloadVideoUrl(null);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}


}