<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Jobs\Video\Parameters;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters\ValidConditionsParameter;

class ValidConditionsTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingMinimumDurationValue()
	{
		$validCondition = new ValidConditionsParameter();

		$duration = 65;

		$validCondition->setMinimumDuration($duration);
		$this->assertEquals($duration, $validCondition->getMinimumDuration());

		$validCondition->setMinimumDuration(-1);
		$this->assertEquals(0, $validCondition->getMinimumDuration());
	}

	public function testSettingOriginalFileSizeValue()
	{
		$fileSize = 800;

		$validCondition = new ValidConditionsParameter();

		$validCondition->setOriginalFileSize($fileSize);
		$this->assertEquals($fileSize, $validCondition->getOriginalFileSize());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Original file size value has to be greater than 0.
	 */
	public function testSettingOriginalFileSizeValueException()
	{
		$validCondition = new ValidConditionsParameter();

		try {
			$validCondition->setOriginalFileSize(0);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

}