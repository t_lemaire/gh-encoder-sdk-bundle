<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Jobs;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\VideoJob;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters\ValidConditionsParameter;

class VideoJobTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingCallbackUrlValue()
	{
		$callback = 'http://www.xxx.com/';

		$upload = new VideoJobMock();

		$this->assertEquals(null, $upload->getCallbackUrl());

		$upload->setCallbackUrl($callback);
		$this->assertEquals($callback, $upload->getCallbackUrl());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Invalid callback url format.
	 */
	public function testSettingCallbackUrlValueException()
	{
		$job = new VideoJobMock();

		try {
			$job->setCallbackUrl(null);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingValidConditionsValue()
	{
		$conditions = new ValidConditionsParameter();
		$conditions->setMinimumDuration(60);
		$conditions->setOriginalFileSize(1234);

		$job = new VideoJobMock();

		$this->assertEquals(null, $job->getValidConditions());

		$job->setValidConditions($conditions);
		$this->assertEquals($conditions, $job->getValidConditions());
	}

	/**
	 * @expectedException \TypeError
	 * @expectedExceptionMessage Argument 1 passed to GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\VideoJob::setValidConditions() must be an instance of GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters\ValidConditionsParameter, null given
	 */
	public function testSettingValidConditionsNullValueException()
	{
		$job = new VideoJobMock();

		$job->setValidConditions(null);
	}

	public function testSettingDuplicateThresholdValue()
	{
		$job = new VideoJobMock();

		//Default value
		$defaultValue = 15.0;
		$this->assertEquals($defaultValue, $job->getDuplicateThreshold());

		$threshold = 20;

		$job->setDuplicateThreshold($threshold);
		$this->assertEquals($threshold, $job->getDuplicateThreshold());

		$job->setDuplicateThreshold();
		$this->assertEquals($defaultValue, $job->getDuplicateThreshold());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Duplicate threshold value must be greater than 0.
	 */
	public function testSettingDuplicateThresholdValueException()
	{
		$job = new VideoJobMock();

		try {
			$job->setDuplicateThreshold(-1);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingAutoAdjustMaxVideoBitRateValue()
	{
		$job = new VideoJobMock();

		//Default value
		$defaultValue = true;
		$this->assertEquals($defaultValue, $job->getAutoAdjustMaxVideoBitRate());

		$adjust = false;

		$job->setAutoAdjustMaxVideoBitRate($adjust);
		$this->assertEquals($adjust, $job->getAutoAdjustMaxVideoBitRate());

		$job->setAutoAdjustMaxVideoBitRate();
		$this->assertEquals($defaultValue, $job->getAutoAdjustMaxVideoBitRate());
	}

	public function testSettingEncoderTagValue()
	{
		$job = new VideoJobMock();

		$tag = VideoJob::ENCODER_TAG_PRODUCTION_CHUNK_UPLOAD;
		$job->setEncoderTag($tag);
		$this->assertEquals($tag, $job->getEncoderTag());

		$tag = 'supertag';
		$job->setEncoderTag($tag);
		$this->assertEquals($tag, $job->getEncoderTag());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Encoder Tag can't be empty.
	 */
	public function testSettingEncoderTagValueException()
	{
		$job = new VideoJobMock();

		$job->setEncoderTag(null);
	}

}

class VideoJobMock extends VideoJob
{

}