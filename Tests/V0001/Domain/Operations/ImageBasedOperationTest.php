<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Operations;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\ImageMetrics;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\TimeLineOperation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;

class ImageBasedOperationTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingFilePatternValue()
	{
		/* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\TimeLineOperation $operation */
        $operation = new TimeLineOperation();
		$filePattern = new FilePattern();
		$filePattern->setBasePath('/local');
		$filePattern->setFilePattern('%y-%m-%d');
		$filePattern->setPathPattern('%y-%m-%d');

        $imageMetrics = new ImageMetrics();
        $imageMetrics->setFilePattern($filePattern);

		$operation->addMetric($imageMetrics);

        /* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\ImageMetrics $metricFromOp */
        $metricFromOp = $operation->getMetrics();
        $this->assertEquals($imageMetrics, $metricFromOp[$imageMetrics->getMetricId()]);

        $filePattern = $metricFromOp[$imageMetrics->getMetricId()]->getFilePattern();
		$this->assertEquals($imageMetrics->getFilePattern(), $filePattern);
	}


	public function testSettingMetrics()
	{
		/* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\TimeLineOperation $operation */
		$operation = new TimeLineOperation();

		$filePattern1 = new FilePattern();
		$filePattern1->setBasePath('/local/metric1');
		$filePattern1->setFilePattern('%y-%m-%d');
		$filePattern1->setPathPattern('%y-%m-%d');

		$imageMetric1 = new ImageMetrics();
		$imageMetric1->setFilePattern($filePattern1);
		$imageMetric1->setMetricId('metric1');

		$filePattern2 = new FilePattern();
		$filePattern2->setBasePath('/local/metric2');
		$filePattern2->setFilePattern('%y-%m-%d');
		$filePattern2->setPathPattern('%y-%m-%d');

		$imageMetric2 = new ImageMetrics();
		$imageMetric2->setFilePattern($filePattern2);
		$imageMetric2->setMetricId('metric2');

		$metrics = array(
			'metric1' => $imageMetric1,
			'metric2' => $imageMetric2
		);

		$operation->setMetrics($metrics);

		$this->assertEquals($metrics, $operation->getMetrics());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Invalid metric content, File Pattern definition is missing.
	 */
	public function testSettingAMetricWithNoFilePattern()
	{
		/* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\TimeLineOperation $operation */
		$operation = new TimeLineOperation();

		$imageMetric = new ImageMetrics();

		$operation->addMetric($imageMetric);
	}

}
