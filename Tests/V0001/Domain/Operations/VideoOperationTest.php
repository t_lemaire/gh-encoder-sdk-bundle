<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Operations;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation;


class VideoOperationTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingUrlValue()
	{
		$url = 'http://www.pornhub.com/';

		/* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
		$operation = $this->createMock('\GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation');

		$this->assertEquals(null, $operation->getUrl());

		$operation->setUrl($url);
		$this->assertEquals($url, $operation->getUrl());
	}

	public function testSettingUrlUtf8Value()
	{
		$url = 'http://見.香港/';

		/* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
		$operation = $this->createMock('\GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation');

		$operation->setUrl($url);
		$this->assertEquals($url, $operation->getUrl());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Url has to be a string value.
	 */
	public function testSettingUrlNullValueException()
	{
		/* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
		$operation = $this->createMock('\GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation');

		try {
			$operation->setUrl(null);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Invalid url format.
	 */
	public function testSettingInvalidUrlValueException()
	{
		/* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
		$operation = $this->createMock('\GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation');

		try {
			$operation->setUrl('hhttp://www.redtube.com/');
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingFilePatternValue()
	{
		/* @var \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation $operation */
		$operation = $this->createMock('\GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation');

		$filePattern = new FilePattern();
		$filePattern->setBasePath('/local');
		$filePattern->setFilePattern('%y-%m-%d');
		$filePattern->setPathPattern('%y-%m-%d');

		$operation->setFilePattern($filePattern);
		$this->assertEquals($filePattern, $operation->getFilePattern());
	}

}
