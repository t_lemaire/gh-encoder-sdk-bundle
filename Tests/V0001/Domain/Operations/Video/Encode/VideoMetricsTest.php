<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Operations\Video\Encode;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\VideoMetrics;

class VideoMetricsTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingBitsPerSecondValue()
	{
		$bits = 10;

		$videoMetrics = new VideoMetrics();

		$videoMetrics->setBitsPerSecond($bits);
		$this->assertEquals($bits, $videoMetrics->getBitsPerSecond());

		$videoMetrics->setBitsPerSecond(null);
		$this->assertEquals(null, $videoMetrics->getBitsPerSecond());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Bits per second value must be greater than 0.
	 */
	public function testSettingBitsPerSecondValueException()
	{
		$videoMetrics = new VideoMetrics();

		try {
			$videoMetrics->setBitsPerSecond(0);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingFramesPerSecondValue()
	{
		$frames = 10;

		$videoMetrics = new VideoMetrics();

		$videoMetrics->setFramesPerSecond($frames);
		$this->assertEquals($frames, $videoMetrics->getFramesPerSecond());

		$videoMetrics->setFramesPerSecond();
		$this->assertEquals(24, $videoMetrics->getFramesPerSecond());

		$videoMetrics->setFramesPerSecond(null);
		$this->assertEquals(null, $videoMetrics->getFramesPerSecond());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Frames per second value must be greater than 0.
	 */
	public function testSettingFramesPerSecondException()
	{
		$channels = 0;

		$videoMetrics = new VideoMetrics();

		try {
			$videoMetrics->setFramesPerSecond($channels);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingWidthValue()
	{
		$width = 10;

		$videoMetrics = new VideoMetrics();

		$videoMetrics->setWidth($width);
		$this->assertEquals($width, $videoMetrics->getWidth());

		$videoMetrics->setWidth(null);
		$this->assertEquals(null, $videoMetrics->getWidth());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Width value must be greater than 0.
	 */
	public function testSettingWidthValueException()
	{
		$videoMetrics = new VideoMetrics();

		try {
			$videoMetrics->setWidth(0);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingHeightValue()
	{
		$height = 10;

		$videoMetrics = new VideoMetrics();

		$videoMetrics->setHeight($height);
		$this->assertEquals($height, $videoMetrics->getHeight());

		$videoMetrics->setHeight(null);
		$this->assertEquals(null, $videoMetrics->getHeight());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Height value must be greater than 0.
	 */
	public function testSettingHeightValueException()
	{
		$videoMetrics = new VideoMetrics();

		try {
			$videoMetrics->setHeight(0);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

}