<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Operations\Video\Encode\Parameters;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Watermark;

class WatermarkTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingHeightRatioValue()
	{
		$ratio = 1.0;

		$watermark = new Watermark();
		$watermark->setHeightRatio($ratio);

		$this->assertEquals($ratio, $watermark->getHeightRatio());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Height Ratio value must be greater than 0.
	 */
	public function testSettingHeightRatioValueException()
	{
		$watermark = new Watermark();

		try {
			$watermark->setHeightRatio(0);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingImageUrlValue()
	{
		$url = 'http://www.youporn.com';
		$tag = null;

		$watermark = new Watermark();
		$watermark->setTag('hello');
		$watermark->setImageUrl($url);

		$this->assertEquals($url, $watermark->getImageUrl());
		$this->assertEquals($tag, $watermark->getTag());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Image Url has to be a string value.
	 */
	public function testSettingImageUrlNullValueException()
	{
		$watermark = new Watermark();

		try {
			$watermark->setImageUrl(null);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Invalid image url format
	 */
	public function testSettingImageUrlValueException()
	{
		$url = 'hello';

		$watermark = new Watermark();

		try {
			$watermark->setImageUrl($url);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingImageUrlUtf8Value()
	{
		$url = 'http://見.香港/';

		$watermark = new Watermark();
		$watermark->setImageUrl($url);

		$this->assertEquals($url, $watermark->getImageUrl());
	}

	public function testSettingTagValue()
	{
		$url = null;
		$tag = 'hello';

		$watermark = new Watermark();
		$watermark->setImageUrl('http://www.youporn.com');
		$watermark->setTag($tag);

		$this->assertEquals($tag, $watermark->getTag());
		$this->assertEquals($url, $watermark->getImageUrl());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Tag has to be a string value.
	 */
	public function testSettingTagNullValueException()
	{
		$watermark = new Watermark();

		try {
			$watermark->setTag(null);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage positionX has to be a valid value.
	 */
	public function testSettingWrongWatermarkValueException(){

		$watermark = new Watermark();
		$watermark->setImageUrl('http://www.youporn.com');
		$watermark->setPositionX('middle');
	}

	public function testSettingWatermarkValue(){

		$watermark = new Watermark();

		$this->assertEquals(Watermark::POSITION_X_RIGHT, $watermark->getPositionX());

		$watermark->setImageUrl('http://www.youporn.com');
		$watermark->setPositionX(Watermark::POSITION_X_LEFT);

		$this->assertEquals(Watermark::POSITION_X_LEFT, $watermark->getPositionX());
	}


}