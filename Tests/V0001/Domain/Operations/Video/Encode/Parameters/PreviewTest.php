<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Operations\Video\Encode\Parameters;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Preview;

class PreviewTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingDefaultDurationValue()
	{
		$duration = 10;

		$preview = new Preview();
		$preview->setDefaultDuration($duration);

		$this->assertEquals($duration, $preview->getDefaultDuration());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Default duration value must be greater than 0.
	 */
	public function testSettingDefaultDurationValueException()
	{
		$preview = new Preview();

		try {
			$preview->setDefaultDuration(0);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingDurationValue()
	{
		$duration = 10;

		$preview = new Preview();
		$preview->setDuration($duration);

		$this->assertEquals($duration, $preview->getDuration());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Duration value must be greater than 0.
	 */
	public function testSettingDurationValueException()
	{
		$preview = new Preview();

		try {
			$preview->setDuration(0);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingStartTimeValue()
	{
		$time = 30;

		$preview = new Preview();

		$preview->setStartTime($time);
		$this->assertEquals($time, $preview->getStartTime());

		$preview->setStartTime(-10);
		$this->assertEquals(0, $preview->getStartTime());
	}

}