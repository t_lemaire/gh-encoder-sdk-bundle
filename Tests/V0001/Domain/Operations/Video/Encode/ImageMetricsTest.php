<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Operations\Video;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\ImageMetrics;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Formats\ImageFormats as ImageFormats;


class ImageMetricsTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingFormatValue()
	{
		$imageMetrics = new ImageMetrics();

        $imageMetrics->setFormat(ImageFormats::FORMAT_JPG);
		$this->assertEquals(ImageFormats::FORMAT_JPG, $imageMetrics->getFormat());

        $imageMetrics->setFormat(ImageFormats::FORMAT_PNG);
		$this->assertEquals(ImageFormats::FORMAT_PNG, $imageMetrics->getFormat());

        $imageMetrics->setFormat(ImageFormats::FORMAT_PNG);
		$this->assertEquals(ImageFormats::FORMAT_PNG, $imageMetrics->getFormat());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Invalid image format, allowed inputs: jpg, png, gif.
	 */
	public function testSettingFormatValueException()
	{
		$imageMetrics = new ImageMetrics();

		try {
			$imageMetrics->setFormat('brand new image format');
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingWidthValue()
	{
		$imageMetrics = new ImageMetrics();

		$w = 100;

		$imageMetrics->setWidth($w);
		$this->assertEquals($w, $imageMetrics->getWidth());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Width value has to be greater than 0 or null.
	 */
	public function testSettingWidthValueException()
	{
		$imageMetrics = new ImageMetrics();

		try {
			$imageMetrics->setWidth(-1);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingHeightValue()
	{
		$imageMetrics = new ImageMetrics();

		$h = 100;

		$imageMetrics->setHeight($h);
		$this->assertEquals($h, $imageMetrics->getHeight());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Height value has to be greater than 0 or null.
	 */
	public function testSettingHeightValueException()
	{
		$imageMetrics = new ImageMetrics();

		try {
			$imageMetrics->setHeight(-1);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingQualityValue()
	{
		$imageMetrics = new ImageMetrics();

		$q = 100;

		$imageMetrics->setQuality($q);
		$this->assertEquals($q, $imageMetrics->getQuality());

		$q = -1;

		$imageMetrics->setQuality($q);
		$this->assertEquals(0, $imageMetrics->getQuality());

		$q = 200;

		$imageMetrics->setQuality($q);
		$this->assertEquals(100, $imageMetrics->getQuality());
	}

}
