<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Operations\Video;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\FlipBookMetrics;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class FlipBookMetricsTest extends \PHPUnit_Framework_TestCase
{

	public function testEnablingImageCompression()
	{
        $flipBookMetric = new FlipBookMetrics();
        $flipBookMetric->setCompression();

		$this->assertEquals(true, $flipBookMetric->getCompression());
	}

	public function testDisablingImageCompression()
	{
        $flipBookMetric = new FlipBookMetrics();

        $flipBookMetric->setCompression(false);
		$this->assertEquals(false, $flipBookMetric->getCompression());
	}

	public function testSettingIntervalValue()
	{
        $flipBookMetric = new FlipBookMetrics();

		$this->assertEquals(null, $flipBookMetric->getInterval());

		$interval = 10.5;

        $flipBookMetric->setInterval($interval);
		$this->assertEquals($interval, $flipBookMetric->getInterval());

		$interval = null;

        $flipBookMetric->setInterval($interval);
		$this->assertEquals($interval, $flipBookMetric->getInterval());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Interval value has to be greater than 0.
	 */
	public function testSettingIntervalValueException()
	{
        $flipBookMetric = new FlipBookMetrics();

		try {
            $flipBookMetric->setInterval('interval');
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingNamePrefixValue()
	{
        $flipBookMetric = new FlipBookMetrics();

		$this->assertEquals('', $flipBookMetric->getNamePrefix());

		$prefix = 'prefix_';

        $flipBookMetric->setNamePrefix($prefix);
		$this->assertEquals($prefix, $flipBookMetric->getNamePrefix());

        $flipBookMetric->setNamePrefix(null);
		$this->assertEquals('', $flipBookMetric->getNamePrefix());
	}


	public function testSettingNumberValue()
	{
        $flipBookMetric = new FlipBookMetrics();

		$this->assertEquals(null, $flipBookMetric->getNumber());

		$number = 10;

        $flipBookMetric->setNumber($number);
		$this->assertEquals($number, $flipBookMetric->getNumber());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Number value has to be greater than 0.
	 */
	public function testSettingNumberValueException()
	{
        $flipBookMetric = new FlipBookMetrics();

		try {
            $flipBookMetric->setNumber('interval');
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingStartOffsetValue()
	{
        $flipBookMetric = new FlipBookMetrics();

		$this->assertEquals(3.0, $flipBookMetric->getStartOffset());

		$startOffset = 10;

        $flipBookMetric->setStartOffset($startOffset);
		$this->assertEquals($startOffset, $flipBookMetric->getStartOffset());

        $flipBookMetric->setStartOffset(-10);
		$this->assertEquals(0, $flipBookMetric->getStartOffset());

        $flipBookMetric->setStartOffset();
		$this->assertEquals(3.0, $flipBookMetric->getStartOffset());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Unable to set the Interval value, a Number value has been defined already, please make sure to use only one of Number or Interval definitions.
	 */
	public function testSettingNumberThenIntervalValueException()
	{
        $flipBookMetric = new FlipBookMetrics();

        $flipBookMetric->setNumber(10);

		try {
            $flipBookMetric->setInterval(10);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Unable to set the Number value, an Interval value has been defined already, please make sure to use only one of Number or Interval definitions.
	 */
	public function testSettingIntervalThenNumberValueException()
	{
        $flipBookMetric = new FlipBookMetrics();

        $flipBookMetric->setInterval(10);

		try {
            $flipBookMetric->setNumber(10);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

}