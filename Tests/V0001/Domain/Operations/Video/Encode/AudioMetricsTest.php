<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Operations\Video\Encode;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\AudioMetrics;
use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class AudioMetricsTest extends \PHPUnit_Framework_TestCase
{

    public function testSettingBitsPerSecondValue()
    {
        $bits = 10;

        $audioMetrics = new AudioMetrics();

        $audioMetrics->setBitsPerSecond($bits);
        $this->assertEquals($bits, $audioMetrics->getBitsPerSecond());

        $audioMetrics->setBitsPerSecond(null);
        $this->assertEquals(null, $audioMetrics->getBitsPerSecond());
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     * @expectedExceptionMessage Bits per second value must be greater than 0.
     */
    public function testSettingBitsPerSecondValueException()
    {
        $audioMetrics = new AudioMetrics();

        try {
            $audioMetrics->setBitsPerSecond(0);
        } catch (InvalidParameterException $e) {
            throw $e;
        }

        $this->fail('An expected exception has not been raised.');
    }

    public function testSettingNumberOfChannelsValue()
    {
        $channels = 10;

        $audioMetrics = new AudioMetrics();

        $audioMetrics->setNumberOfChannels($channels);
        $this->assertEquals($channels, $audioMetrics->getNumberOfChannels());

        $audioMetrics->setNumberOfChannels(null);
        $this->assertEquals(null, $audioMetrics->getNumberOfChannels());
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     * @expectedExceptionMessage Number of channels value must be non-negative
     */
    public function testSettingNumberOfChannelsValueException()
    {
        $audioMetrics = new AudioMetrics();
        $audioMetrics->setNumberOfChannels(-1);
    }

    public function testSettingSamplesPerSecondValue()
    {
        $samples = 10;

        $audioMetrics = new AudioMetrics();

        $audioMetrics->setSamplesPerSecond($samples);
        $this->assertEquals($samples, $audioMetrics->getSamplesPerSecond());

        $audioMetrics->setSamplesPerSecond(null);
        $this->assertEquals(null, $audioMetrics->getSamplesPerSecond());
    }

    /**
     * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
     * @expectedExceptionMessage Samples per second value must be greater than 0.
     */
    public function testSettingSamplesPerSecondException()
    {
        $audioMetrics = new AudioMetrics();
        $audioMetrics->setSamplesPerSecond(0);
    }

}