<?php
namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain\Operations\Video;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\EncodeOperation;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\AudioMetrics;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\VideoMetrics;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Preview;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Watermark;

class EncodeTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingAudioCodecValue()
	{
		$encoding = new EncodeOperation();

		$codec = null;

		$encoding->setAudioCodec($codec);
		$this->assertEquals(null, $encoding->getAudioCodec());

		$codec = 'mp3';

		$encoding->setAudioCodec($codec);
		$this->assertEquals(EncodeOperation::AUDIO_CODEC_COPY, $encoding->getAudioCodec());
	}

	public function testSettingAudioLimitsValue()
	{
		$audioMetrics = new AudioMetrics();
		$audioMetrics->setBitsPerSecond(100);
		$audioMetrics->setNumberOfChannels(16);
		$audioMetrics->setSamplesPerSecond(8);

		$encoding = new EncodeOperation();

		$encoding->setAudioLimits($audioMetrics);
		$this->assertEquals($audioMetrics, $encoding->getAudioLimits());
	}

	/**
	 * @expectedException \TypeError
	 * @expectedExceptionMessage Argument 1 passed to GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\EncodeOperation::setAudioLimits() must be an instance of GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\AudioMetrics, null given
	 */
	public function testSettingAudioLimitsNullValue()
	{
		$encoding = new EncodeOperation();

		$encoding->setAudioLimits(null);
	}

	public function testSettingEncodingModeValue()
	{
		$encoding = new EncodeOperation();

		$encoding->setEncodingMode(EncodeOperation::ENCODING_MODE_VBR);
		$this->assertEquals(EncodeOperation::ENCODING_MODE_VBR, $encoding->getEncodingMode());

		$encoding->setEncodingMode(EncodeOperation::ENCODING_MODE_2PASS);
		$this->assertEquals(EncodeOperation::ENCODING_MODE_2PASS, $encoding->getEncodingMode());

		$encoding->setEncodingMode(EncodeOperation::ENCODING_MODE_1PASS_CRF);
		$this->assertEquals(EncodeOperation::ENCODING_MODE_1PASS_CRF, $encoding->getEncodingMode());

		$encoding->setEncodingMode(EncodeOperation::ENCODING_MODE_1PASS);
		$this->assertEquals(EncodeOperation::ENCODING_MODE_1PASS, $encoding->getEncodingMode());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Invalid encoding mode value, allowed values are: 1pass, 1pass-crf, 2pass, vbr
	 */
	public function testSettingEncodingModeValueException()
	{
		$encoding = new EncodeOperation();

		try {
			$encoding->setEncodingMode('super-encoding-mode');
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingFormatValue()
	{
		$format = 'mp4';

		$encoding = new EncodeOperation();
		$encoding->setFormat($format);

		$this->assertEquals($format, $encoding->getFormat());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Format has to be a string value.
	 */
	public function testSettingFormatValueException()
	{
		$encoding = new EncodeOperation();

		try {
			$encoding->setFormat(null);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingIntroTagValue()
	{
		$encoding = new EncodeOperation();

		$tag = 'intro';

		$encoding->setIntroTag($tag);
		$this->assertEquals($tag, $encoding->getIntroTag());

		$tag = null;

		$encoding->setIntroTag($tag);
		$this->assertEquals($tag, $encoding->getIntroTag());
	}

	public function testSettingOutroTagValue()
	{
		$encoding = new EncodeOperation();

		$tag = 'outro';

		$encoding->setOutroTag($tag);
		$this->assertEquals($tag, $encoding->getOutroTag());

		$tag = null;

		$encoding->setOutroTag($tag);
		$this->assertEquals($tag, $encoding->getOutroTag());
	}

	public function testGettingMandatoryValue()
	{
		$encoding = new EncodeOperation();
		$this->assertEquals(false, $encoding->getMandatory());
	}

	public function testSettingNamePrefixValue()
	{
		$encoding = new EncodeOperation();

		$prefix = 'prefix_';

		$encoding->setNamePrefix($prefix);
		$this->assertEquals($prefix, $encoding->getNamePrefix());

		$encoding->setNamePrefix(null);
		$this->assertEquals('', $encoding->getNamePrefix());
	}

	public function testSettingNameSuffixValue()
	{
		$encoding = new EncodeOperation();

		$suffix = '_suffix';

		$encoding->setNameSuffix($suffix);
		$this->assertEquals($suffix, $encoding->getNameSuffix());

		$encoding->setNameSuffix(null);
		$this->assertEquals('', $encoding->getNameSuffix());
	}

	public function testSettingPreviewValue()
	{
		$preview = new Preview();
		$preview->setDefaultDuration(10);
		$preview->setDuration(20);
		$preview->setStartTime(5);

		$encoding = new EncodeOperation();

		$encoding->setPreview($preview);
		$this->assertEquals($preview, $encoding->getPreview());

		$encoding->setPreview(null);
		$this->assertEquals(null, $encoding->getPreview());
	}


	public function testSettingVideoLimitsValue()
	{
		$metrics = new VideoMetrics();
		$metrics->setBitsPerSecond(8);
		$metrics->setWidth(150);
		$metrics->setHeight(100);
		$metrics->setFramesPerSecond(30);

		$encoding = new EncodeOperation();

		$encoding->setVideoLimits($metrics);
		$this->assertEquals($metrics, $encoding->getVideoLimits());
	}

	/**
	 * @expectedException \TypeError
	 * @expectedExceptionMessage Argument 1 passed to GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\EncodeOperation::setVideoLimits() must be an instance of GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\VideoMetrics, null given
	 */
	public function testSettingVideoLimitsNullValue()
	{
		$encoding = new EncodeOperation();
		$encoding->setVideoLimits(null);
	}

	public function testSettingWatermarkValue()
	{
		$watermark = new Watermark();
		$watermark->setTag('tag');

		$encoding = new EncodeOperation();

		$encoding->setWatermark($watermark);
		$this->assertEquals($watermark, $encoding->getWatermark());

		$encoding->setWatermark(null);
		$this->assertEquals(null, $encoding->getWatermark());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Watermark must contain exactly one of tag and imageUrl defined.
	 */
	public function testSettingWatermarkValueException()
	{
		$watermark = new Watermark();

		$encoding = new EncodeOperation();

		try {
			$encoding->setWatermark($watermark);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingLumaValue()
	{
		$encoding = new EncodeOperation();

		$luma = 1.5;
		$encoding->setLuma($luma);
		$this->assertEquals($luma, $encoding->getLuma());


		$luma = -1.5;
		$encoding->setLuma($luma);
		$this->assertEquals($luma, $encoding->getLuma());
	}

	public function testSettingProfileValue()
	{
		$encoding = new EncodeOperation();

		$profile = EncodeOperation::PROFILE_HIGH;
		$encoding->setProfile($profile);
		$this->assertEquals($profile, $encoding->getProfile());

		$profile = EncodeOperation::PROFILE_NONE;
		$encoding->setProfile($profile);
		$this->assertEquals($profile, $encoding->getProfile());

		$profile = 'profile';
		$encoding->setProfile($profile);
		$this->assertEquals(EncodeOperation::PROFILE_NONE, $encoding->getProfile());
	}

}