<?php

namespace GorillaHub\SDKs\EncodeBundle\Tests\V0001\Domain;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;

class OperationTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingIdValue()
	{
		$id = 'id';

		$operation = new OperationMock();
		$operation->setOperationId($id);

		$this->assertEquals($id, $operation->getOperationId());
	}

	public function testSettingStatusValue()
	{
		$status = 'status';

		$operation = new OperationMock();
		$operation->setStatus($status);

		$this->assertEquals($status, $operation->getStatus());
	}

	/**
	 * @expectedException \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @expectedExceptionMessage Status has to be a string value.
	 */
	public function testSettingStatusValueException()
	{
		$operation = new OperationMock();

		try {
			$operation->setStatus(null);
		} catch (InvalidParameterException $e) {
			throw $e;
		}

		$this->fail('An expected exception has not been raised.');
	}

	public function testSettingPriorityValue()
	{
		$operation = new OperationMock();

		$operation->setPriority(Operation::PRIORITY_HIGH);
		$this->assertEquals(Operation::PRIORITY_HIGH, $operation->getPriority());
	}

}

class OperationMock extends Operation
{

}