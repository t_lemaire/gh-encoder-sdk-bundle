<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Calls;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;
use GorillaHub\SDKs\UploadBundle\V0001\Domain\CSAIMatchingClip;

/**
 * A call of this type is sent to the site's subscription callback URL, which is specified in the database of the
 * upload service.
 */
class DetectCSAIUpdateCall implements SDKCallInterface
{

    /** @var Signature */
    private $signature;

    /**
     * @var string|null The custom ID, if any, that was specified by calling setCustomId() of the original
     *      SubscribeToDetectCSAIOperation object.
     */
    private $customId;

    /**
     * @var string[] The names of the matching jobs, according to Google.
     */
    private $names;

    /** @var CSAIMatchingClip[] Zero or more matching clips. */
    private $matchingClips;

    /**
     * @var string[] The raw responses from Google for the matching jobs.  The client must save these to its database
     *      for future reference even if (ESPECIALLY IF) the result was NOT a match.
     */
    private $rawResponses;

    /**
     * @return string[] @see $names
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param string[] $names @see $names
     * @return $this
     */
    public function setNames($names)
    {
        $this->names = $names;
        return $this;
    }

    /**
     * @return CSAIMatchingClip[] @see $matchingClips
     */
    public function getMatchingClips()
    {
        return $this->matchingClips;
    }

    /**
     * @param CSAIMatchingClip[] $matchingClips @see $matchingClips
     * @return $this
     */
    public function setMatchingClips($matchingClips)
    {
        $this->matchingClips = $matchingClips;
        return $this;
    }

    /**
     * @return string[] @see $rawResponses
     */
    public function getRawResponses()
    {
        return $this->rawResponses;
    }

    /**
     * @param string[] $rawResponses @see $rawResponses
     * @return $this
     */
    public function setRawResponses($rawResponses)
    {
        $this->rawResponses = $rawResponses;
        return $this;
    }



    /** @return bool True if at least one match was found, false otherwise. */
    public function wasMatchFound() {
        return $this->matchingClips !== [];
    }

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return $this
     */
    public function setSignature(Signature $signature) {
        $this->signature = $signature;
        return $this;
    }

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature() {
        return $this->signature;
    }


    /**
     * @return null|string  @see $customId
     */
    public function getCustomId()
    {
        return $this->customId;
    }

    /**
     * @param null|string $customId @see $customId
     * @return $this
     */
    public function setCustomId($customId)
    {
        $this->customId = $customId;
        return $this;
    }






}
