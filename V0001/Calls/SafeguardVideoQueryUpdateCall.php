<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Calls;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\Results\Video\SafeguardVideoQueryMatch;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

/**
 * A call of this type is sent to the site's subscription callback URL, which is specified in the database of the
 * upload service.
 */
class SafeguardVideoQueryUpdateCall implements SDKCallInterface
{

    /** @var string No match was found. */
    const OUTCOME_CLEAR = "clear";

    /** @var string A match was found. */
    const OUTCOME_MATCH = "match";

    /** @var string All available algorithms were tried but no conclusive result was obtained. */
    const OUTCOME_SUSPICIOUS = "suspicious";



    /** @var Signature */
    private $signature;

    /**
     * @var string|null The custom ID, if any, that was specified by calling setCustomId() of the original
     *      SubscribeToSafeguardVideoQueryOperation object.
     */
    private $customId;


    /** @var string One of the OUTCOME_* constants. */
    private $outcome;

    /**
     * @var SafeguardVideoQueryMatch[]
     */
    private $matches;


    /**
     * @var object The "result" object from Safeguard.  This can be archived for diagnostic purposes but it is
     *      not recommended that you try to interpret it directly, because the IDs do not match with what was
     *      specified by the client.
     */
    private $result;

    /** @var string The ID assigned by Safeguard to this query. */
    private $safeguardQueryToken;


    /**
     * @var bool True iff any of the matches satisfy the current "standard" threshold setting (which is set uniformly
     *      for all clients in the Tubes CMS).
     */
    private $standardThresholdMatch = false;

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return $this
     */
    public function setSignature(Signature $signature) {
        $this->signature = $signature;
        return $this;
    }

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature() {
        return $this->signature;
    }


    /**
     * @return null|string  @see $customId
     */
    public function getCustomId()
    {
        return $this->customId;
    }

    /**
     * @param null|string $customId @see $customId
     * @return $this
     */
    public function setCustomId($customId)
    {
        $this->customId = $customId;
        return $this;
    }


    /**
     * @return string One of the OUTCOME_* constants.
     */
    public function getOutcome()
    {
        return $this->outcome;
    }


    /**
     * @param string $outcome One of the OUTCOME_* constants.
     * @return $this
     */
    public function setOutcome($outcome)
    {
        $this->outcome = $outcome;
        return $this;
    }


    /**
     * @return SafeguardVideoQueryMatch[]
     */
    public function getMatches()
    {
        return $this->matches;
    }


    /**
     * @param SafeguardVideoQueryMatch[] $matches
     * @return $this
     */
    public function setMatches($matches)
    {
        $this->matches = $matches;
        return $this;
    }


    /**
     * @return object The "result" object from Safeguard.  This can be archived for diagnostic purposes but it is
     *      not recommended that you try to interpret it directly, because the IDs do not match with what was
     *      specified by the client.
     */
    public function getResult()
    {
        return $this->result;
    }


    /**
     * @param object $result The "result" object from Safeguard.  This can be archived for diagnostic purposes but it
     *      is not recommended that you try to interpret it directly, because the IDs do not match with what was
     *      specified by the client.
     * @return $this
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }


    /**
     * @return string The ID assigned by Safeguard to this query.
     */
    public function getSafeguardQueryToken()
    {
        return $this->safeguardQueryToken;
    }

    /**
     * @param string $safeguardQueryToken The ID assigned by Safeguard to this query.
     * @return $this
     */
    public function setSafeguardQueryToken($safeguardQueryToken)
    {
        $this->safeguardQueryToken = $safeguardQueryToken;
        return $this;
    }


    /**
     * @return boolean True iff any of the matches satisfy the current "standard" threshold setting (which is set
     *      uniformly for all clients in the Tubes CMS).
     */
    public function getStandardThresholdMatch()
    {
        return $this->standardThresholdMatch;
    }

    /**
     * @param boolean $standardThresholdMatch True iff any of the matches satisfy the current "standard" threshold
     *      setting (which is set uniformly for all clients in the Tubes CMS).
     * @return $this
     */
    public function setStandardThresholdMatch($standardThresholdMatch)
    {
        $this->standardThresholdMatch = $standardThresholdMatch;
        return $this;
    }
}
