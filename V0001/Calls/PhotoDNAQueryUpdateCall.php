<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Calls;

use GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;

/**
 * A call of this type is sent to the site's subscription callback URL, which is specified in the database of the
 * upload service.
 */
class PhotoDNAQueryUpdateCall implements SDKCallInterface
{


    /** @var Signature */
    private $signature;

    /**
     * @var string|null The custom ID, if any, that was specified by calling setCustomId() of the original
     *      SubscribeToPhotoDNAQueryOperation object.
     */
    private $customId;

    /** @var bool True iff there was a match. */
    private $isMatch;

    /**
     * @var string The uninterpreted response from the PhotoDNA service.
     */
    private $rawResponse;

    /**
     * @var string An ID assigned to this job by the PhotoDNA service.
     */
    private $trackingId;


    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return $this
     */
    public function setSignature(Signature $signature) {
        $this->signature = $signature;
        return $this;
    }

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    public function getSignature() {
        return $this->signature;
    }


    /**
     * @return string|null The custom ID, if any, that was specified by calling setCustomId() of the original
     *      SubscribeToPhotoDNAQueryOperation object.
     */
    public function getCustomId()
    {
        return $this->customId;
    }

    /**
     * @param string|null The custom ID, if any, that was specified by calling setCustomId() of the original
     *      SubscribeToPhotoDNAQueryOperation object.
     * @return $this
     */
    public function setCustomId($customId)
    {
        $this->customId = $customId;
        return $this;
    }

    /**
     * @return bool True iff there was a match.
     */
    public function getIsMatch()
    {
        return $this->isMatch;
    }

    /**
     * @param bool $isMatch True iff there was a match.
     * @return $this
     */
    public function setIsMatch($isMatch)
    {
        $this->isMatch = $isMatch;
        return $this;
    }

    /**
     * @return string The uninterpreted response from the PhotoDNA service.
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * @param string $rawResponse The uninterpreted response from the PhotoDNA service.
     * @return $this
     */
    public function setRawResponse($rawResponse)
    {
        $this->rawResponse = $rawResponse;
        return $this;
    }

    /**
     * @return string An ID assigned to this job by the PhotoDNA service.
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * @param string $trackingId An ID assigned to this job by the PhotoDNA service.
     * @return $this
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;
        return $this;
    }




}
