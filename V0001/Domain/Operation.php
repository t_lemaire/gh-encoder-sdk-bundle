<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

/**
 * class Operation
 *
 * @package GorillaHub\SDKs\EncodeBundle\V0001\Domain
 */
abstract class Operation implements OperationInterface
{

	const PRIORITY_LOW = 1;
	const PRIORITY_MEDIUM = 5;
	const PRIORITY_HIGH = 10;


    const ENCODER_TAG_DEFAULT          = 'default';
    const ENCODER_TAG_PRODUCTION_AMSV2 = 'amsv2';
    const ENCODER_TAG_PRODUCTION_AMSV2_REENCODE = 'reencode';
    const ENCODER_TAG_PRODUCTION_AMSV2_SPAM = 'amsv2_spam';
    const ENCODER_TAG_PRODUCTION_VIDEOLUBE = 'videolube';
    const ENCODER_TAG_PRODUCTION_VIDEOLUBE_REENCODE = 'videolube_reencode';
    const ENCODER_TAG_PRODUCTION_MGLUBE = 'mglube';
    const ENCODER_TAG_PRODUCTION_MGLUBE_RETRY = 'mglube_retry';
    const ENCODER_TAG_PRODUCTION_MGLUBE_REENCODE = 'mglube_reencode';
    const ENCODER_TAG_PRODUCTION_MGLUBE_REENCODE_VR = 'mglube_reencode_vr';
    const ENCODER_TAG_PRODUCTION_MGLUBE_REENCODE_RETRY = 'mglube_reencode_retry';

	/**
	 * @var string
	 */
	private $operationId;

	/**
	 * @var string
	 */
	private $status;

	/**
	 * @var int
	 */
	protected $priority = 5;

	/**
	 * @var string
	 */
	protected $customId;

    /**
     * @var  string
     */
    protected $encoderTag = self::ENCODER_TAG_DEFAULT;

	/**
	 * @var string|null The name of the storage (from `storage_volume_aliases`.`alias`) to which to write the
	 * 		intermediate files, or "default" to write to the default space for the client, or null to let the upload
	 * 		service figure out the best storage to use.
	 */
	protected $volumeName = null;


	/**
	 * @inheritdoc
	 */
	final public function setOperationId($id)
	{
		$this->operationId = $id;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	final public function getOperationId()
	{
		return $this->operationId;
	}

	/**
	 * @inheritdoc
	 */
	final public function setCustomId($id)
	{
		$this->customId = $id;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	final public function getCustomId()
	{
		return $this->customId;
	}

	/**
	 * @inheritdoc
	 */
	final public function setStatus($status)
	{
		if (!is_string($status)) {
			throw new InvalidParameterException('Status has to be a string value.');
		}

		$this->status = $status;
		return $this;

	}

	/**
	 * @inheritdoc
	 */
	final public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @inheritdoc
	 */
	final public function setPriority($priority)
	{
		$this->priority = (int) $priority;
		return $this;

	}

	/**
	 * @inheritdoc
	 */
	final public function getPriority()
	{
		return $this->priority;
	}

    /**
     * @inheritdoc
     */
    final public function setEncoderTag($encoderTag)
    {
        $this->encoderTag =  $encoderTag;
        return $this;

    }

    /**
     * @inheritdoc
     */
    final public function getEncoderTag()
    {
        return $this->encoderTag;
    }

	/**
	 * @return string|null @see $volumeName
	 */
	public function getVolumeName()
	{
		return $this->volumeName;
	}

	/**
	 * @param string|null $volumeName @see $volumeName
	 * @return $this
	 */
	public function setVolumeName($volumeName)
	{
		$this->volumeName = $volumeName;
		return $this;
	}

	/**
	 * @return string The explicitly defined volume name (as returned by getVolumeName()) or, if getVolumeName()
	 * 		would return null, the volume to which the output files are to be saved.  If this is not known, the
	 * 		return value is "default".
	 */
	public function autoSelectVolumeName() {
		return $this->getVolumeName() !== null ? $this->getVolumeName() : 'default';
	}

}
