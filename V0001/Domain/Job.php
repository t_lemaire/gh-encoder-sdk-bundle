<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\DateTimeInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

/**
 * Class Job
 * @Annotation
 *
 * @package GorillaHub\SDKs\EncodeBundle\V0001\Domain
  */
abstract class Job implements JobInterface
{

	/**
	 * @var Signature
	 */
	protected $signature;

	/**
	 * @var OperationInterface[]
	 */
	protected $operations = array();

	/**
	 * Operations to be performed on resulting files
	 *
	 * @var OperationInterface[]
	 */
	protected $postOperations = array();

	/**
	 * @var \GorillaHub\SDKs\SDKBundle\V0001\Domain\DateTimeInterface
	 */
	private $dateTime;

	/**
	 * @var string
	 */
	private $customId;

	/**
	 * @param string $id
	 *
	 * @return $this
	 */
	final public function setCustomId($id)
	{
		$this->customId = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getCustomId()
	{
		return $this->customId;
	}

	/**
	 * Sets the signature.
	 *
	 * @param Signature $signature
	 *
	 * @return self
	 */
	final public function setSignature(Signature $signature)
	{
		$this->signature = $signature;

		return $this;
	}

	/**
	 * Returns the signature.
	 *
	 * @return Signature
	 */
	final public function getSignature()
	{
		return $this->signature;
	}

	/**
	 * @inheritdoc
	 */
	public function addOperation(OperationInterface $operation)
	{
		$operationId = $operation->getOperationId();

		if ($operationId === null || mb_strlen($operationId) == 0) {
			$operation->setOperationId(md5(uniqid('', true)));
		} else {
			if (strlen($operationId) > 32) {
				throw new InvalidParameterException('Operation Id must be maximum 32 characters long.');
			}
		}

		$this->operations[] = $operation;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function setOperations(array $operations)
	{
		$this->operations = array();

		foreach ($operations as $operation) {
			if ($operation instanceof \GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface) {
				$this->addOperation($operation);
			}
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	final public function getOperations()
	{
		return $this->operations;
	}

	/**
	 * @param string $operationId
	 * @return OperationInterface
	 */
	public function getOperation($operationId) {
		foreach ($this->operations as $operation) {
			if ($operation->getOperationId() === $operationId) {
				return $operation;
			}
		}
		return null;
	}

	/**
	 * @return int
	 */
	final public function countOperations()
	{
		return count($this->operations);
	}

	/**
	 * @inheritdoc
	 */
	public function addPostOperation(OperationInterface $operation)
	{
		$operationId = $operation->getOperationId();

		if ($operationId === null || mb_strlen($operationId) == 0) {
			$operation->setOperationId(md5(uniqid('', true)));
		}

		$this->postOperations[] = $operation;
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function setPostOperations($postOperations)
	{
		$this->postOperations = array();

		foreach ($postOperations as $operation) {
			if ($operation instanceof \GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface) {
				$this->addPostOperation($operation);
			}
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	final public function getPostOperations()
	{
		return $this->postOperations;
	}

	/**
	 * @return int
	 */
	final public function countPostOperations()
	{
		return count($this->postOperations);
	}

	/**
	 * Sets the Date Time representation of the job
	 *
	 * @param DateTimeInterface $dateTime
	 *
	 * @return $this
	 */
	final public function setDateTime(DateTimeInterface $dateTime)
	{
		$this->dateTime = $dateTime;

		return $this;
	}

	/**
	 * Returns the date time representation of the job
	 *
	 * @return DateTimeInterface
	 */
	final public function getDateTime()
	{
		return $this->dateTime;
	}

}