<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\EncodeOperation;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\VobileOperation;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\VideoJob;

/**
 * Class VideoEncodeJob
 *
 * @Annotation
 *
 * @package GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video
 */
class VideoEncodeJob extends VideoJob
{

	/**
	 * The video download url for the encoder
	 *
	 * @var string
	 */
	private $downloadVideoUrl;

	/**
	 * @var string|null The name of the encoder queue to use, or null to use the default queue for the client.
	 *		You can invent any queue name you want and set it here.
	 */
	private $queueName;

	/**
	 * Sets the download video url for the encoder.
	 *
	 * @param $url
	 * @return self
	 * @throws InvalidParameterException
	 */
	final public function setDownloadVideoUrl($url)
	{
		if (mb_strpos($url, 'http://') === 0 || mb_strpos($url, 'https://') === 0) {
			$this->downloadVideoUrl = $url;
		} else {
			throw new InvalidParameterException('Invalid download video url format.');
		}

		return $this;
	}

	/**
	 * Return the download video url
	 *
	 * @return string
	 */
	final public function getDownloadVideoUrl()
	{
		return $this->downloadVideoUrl;
	}

	/**
	 * @return VobileOperation The first Vobile operation, or null if none.
	 */
	public function getVobileOperation() {
		foreach ($this->operations as $operation) {
			if ($operation instanceof VobileOperation) {
				return $operation;
			}
		}
		return null;
	}


	/**
	 * @return null|string @see $queueName
	 */
	public function getQueueName()
	{
		return $this->queueName;
	}

	/**
	 * @param null|string $queueName @see $queueName
	 * @return $this
	 */
	public function setQueueName($queueName)
	{
		$this->queueName = $queueName;
		return $this;
	}

    /**
     * @return EncodeOperation[] All video encode operations.
     */
    public function getEncodeOperations()
    {
        $operations = [];
        foreach ($this->operations as $operation) {
            if ($operation instanceof EncodeOperation) {
                $operations[] = $operation;
            }
        }
        return $operations;
    }

}