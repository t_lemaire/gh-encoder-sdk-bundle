<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class ValidConditionsParameter
{

	/**
	 * The minimum acceptable size of the input video, in seconds.
	 * The job will fail if the original video is shorter than this.
	 *
	 * @var int
	 */
	private $minimumDuration = 0;

	/**
	 * The expected size of the original file, after downloading.
	 * The encoder will consider itself to be unable to download the video if it does not receive a file of exactly this size.
	 *
	 * @var int|null
	 */
	private $originalFileSize = null;

	/**
	 * Sets the minimum acceptable size of the input video, in seconds.
	 * The job will fail if the original video is shorter than this.
	 *
	 * @param int $minimumDuration
	 * @return self
	 */
	final public function setMinimumDuration($minimumDuration)
	{
		$minimumDuration = (int) $minimumDuration;

		if ($minimumDuration < 0) {
			$minimumDuration  = 0;
		}

		$this->minimumDuration = $minimumDuration;

		return $this;
	}

	/**
	 * Returns the minimum duration value.
	 *
	 * @return float
	 */
	final public function getMinimumDuration()
	{
		return $this->minimumDuration;
	}

	/**
	 * Sets the expected size of the original file, after downloading.
	 * The encoder will consider itself to be unable to download the video if it does not receive a file of exactly this size.
	 *
	 * @param int $originalFileSize
	 * @return self
	 * @throws InvalidParameterException
	 */
	final public function setOriginalFileSize($originalFileSize)
	{
		if (null !== $originalFileSize) {
			$originalFileSize = (int) $originalFileSize;

			if ($originalFileSize <= 0) {
				throw new InvalidParameterException('Original file size value has to be greater than 0.');
			}

			$this->originalFileSize = $originalFileSize;
		}

		return $this;
	}

	/**
	 * Returns the original file size value.
	 *
	 * @return int|null
	 */
	final public function getOriginalFileSize()
	{
		return $this->originalFileSize;
	}

}