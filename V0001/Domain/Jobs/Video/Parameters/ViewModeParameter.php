<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

/**
 * An instance of this class describes the kind of viewer that the video is intended for: e.g. flat or VR, mono or
 * stereo.  Default is a regular mono, flat video.
 */
class ViewModeParameter
{
	const STEREOMODE_LEFT_RIGHT = 'left-right';
	const STEREOMODE_ABOVE_BELOW = 'above-below';
	const STEREOMODE_MONO = 'mono';

	const PROJECTIONMAP_FLAT = 'flat';
	const PROJECTIONMAP_VR = 'vr';

	private $stereoMode = self::STEREOMODE_MONO;

	private $projectionMap = self::PROJECTIONMAP_FLAT;

	final public function __construct() {
		$this->stereoMode = self::STEREOMODE_MONO;
		$this->projectionMap = self::PROJECTIONMAP_FLAT;
	}

	/**
	 * @param string $stereoMode The stereoscopy mode; one of the STEREOMODE_* constants.
	 * @return $this
	 * @throws InvalidParameterException
	 */
	final public function setStereoMode($stereoMode) {
		if ($this->isStereoModeValid($stereoMode) === false) {
			throw new InvalidParameterException("Invalid stereo mode: " . $stereoMode);
		}
		$this->stereoMode = $stereoMode;
		return $this;
	}

	final public function getStereoMode() {
		return $this->stereoMode;
	}

	/**
	 * @param string $projectionMap One of the PROJECTIONMAP_* constants.
	 * @return $this
	 * @throws InvalidParameterException
	 */
	final public function setProjectionMap($projectionMap) {
		if ($this->isProjectionMapValid($projectionMap) === false) {
			throw new InvalidParameterException("Invalid projection map: " . $projectionMap);
		}
		$this->projectionMap = $projectionMap;
		return $this;
	}

	final public function getProjectionMap() {
		return $this->projectionMap;
	}

	final public function isStereoModeValid($stereoMode)
	{
		$stereoModes = [
			self::STEREOMODE_LEFT_RIGHT,
			self::STEREOMODE_ABOVE_BELOW,
			self::STEREOMODE_MONO
		];
		return in_array($stereoMode, $stereoModes);
	}

	final public function isProjectionMapValid($projectionMap)
	{
		$projectionMaps = [
			self::PROJECTIONMAP_FLAT,
			self::PROJECTIONMAP_VR
		];
		return in_array($projectionMap, $projectionMaps);
	}
}
