<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters\ViewModeParameter;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\VideoMetrics;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Job;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Jobs\Video\Parameters\ValidConditionsParameter;

abstract class VideoJob extends Job
{

	/**
	 * Tags in descending order of priority:
	 *
	 * Urgent:                 For QA or jobs escalated by management. Very few jobs should be URGENT.
	 *                         (If you abuse this, your access to this tag will be removed.)
	 * Paid upload:            (Restricted*) For new uploads by paying users.
	 * Paid upload retry:      (Restricted*) For retrying a paid upload.
	 * Partner upload:         (Restricted*) For new uploads by content partners.
	 * Partner upload retry:   (Restricted*) For retrying a content partner upload.
	 * New upload:             For new uploads by regular users.
	 * New upload retry:       For retrying a regular upload.
	 * Reencode:               For reencoding a video that's already online.
	 * Reencode retry:         For retrying a reencode.
	 * Spam:                   For new uploads by recognized spammers, or a new uploads that seem like duplicates.
	 *
	 * Restricted tags are usable only by some sites.
	 */
	const ENCODER_TAG_URGENT = 'urgent';
	const ENCODER_TAG_PAID_UPLOAD = 'paid_upload';
	const ENCODER_TAG_PAID_UPLOAD_RETRY = 'paid_upload_retry';
	const ENCODER_TAG_PARTNER_UPLOAD = 'partner_upload';
	const ENCODER_TAG_PARTNER_UPLOAD_RETRY = 'partner_upload_retry';
	const ENCODER_TAG_NEW_UPLOAD = 'new_upload';
	const ENCODER_TAG_NEW_UPLOAD_RETRY = 'new_upload_retry';
	const ENCODER_TAG_REENCODE = 'reencode';
	const ENCODER_TAG_REENCODE_RETRY = 'reencode_retry';
	const ENCODER_TAG_SPAM = 'spam';

	# Deprecated tags:
	const ENCODER_TAG_PRIORITY_UPLOAD = 'priority_upload';
	const ENCODER_TAG_PRODUCTION_AMSV2 = 'amsv2';
	const ENCODER_TAG_PRODUCTION_AMSV2_GAYTUBE = 'amsv2_gaytube';
	const ENCODER_TAG_PRODUCTION_AMSV2_SPAM = 'amsv2_spam';
	const ENCODER_TAG_PRODUCTION_AMSV2_WCAMS = 'amsv2_wcams';
	const ENCODER_TAG_PRODUCTION_AMSV2_XTUBE = 'amsv2_xtube';
	const ENCODER_TAG_PRODUCTION_CHIV2 = 'chiV2';
	const ENCODER_TAG_PRODUCTION_CHUNK_UPLOAD = 'chunk_upload';
	const ENCODER_TAG_PRODUCTION_VIDEOLUBE = 'videolube';
	const ENCODER_TAG_PRODUCTION_MGLUBE = 'mglube';
	const ENCODER_TAG_PRODUCTION_MGLUBE_RETRY = 'mglube_retry';
	const ENCODER_TAG_PRODUCTION_MGLUBE_REENCODE = 'mglube_reencode';
	const ENCODER_TAG_PRODUCTION_MGLUBE_REENCODE_VR = 'mglube_reencode_vr';
	const ENCODER_TAG_PRODUCTION_MGLUBE_REENCODE_RETRY = 'mglube_reencode_retry';
	const ENCODER_TAG_STAGE_STAGEV2 = 'stagev2';
	
	const ENVIRONMENT_MGLUBE = 'mglube';
	const ENVIRONMENT_PROD = 'prod';
	const ENVIRONMENT_BETA = 'beta';
	const ENVIRONMENT_REALITY_LIVE = 'reality-live';

	const ENCODING_SERVICE_LITMUS = 'encoding_service';
	const ENCODING_SERVICE_2016 = 'encoder_2016';
	const ENCODING_SERVICE_ENCODER_API = 'encoding_service_encoder_api';
	const ENCODING_SERVICE_MGLUBE = 'mglube';

	/**
	 * The conditions for the video to be valid.
	 *
	 * @var ValidConditionsParameter
	 */
	private $validConditions;

	/**
	 * The maximum percentage difference in metrics (e.g. resolution) where the metrics are considered identical.
	 *
	 * @var float
	 */
	private $duplicateThreshold = 15.0; //Check with James if this is getting use or not

	/**
	 * True if the bit rate is limited proportionally whenever the resolution is limited, false otherwise.
	 *
	 * @var bool
	 */
	private $autoAdjustMaxVideoBitRate = true;

	/**
	 * The url to call back the site after receiving the response from the encoder.
	 *
	 * @var string
	 */
	private $callbackUrl;

	/**
	 * Is the encoder workers that will be used to encode the job
	 *
	 * @var string
	 */
	private $encoderTag;

	/**
	 * @var VideoMetrics|null The metrics of the original video, or null if not known.
	 */
	private $originalVideoMetrics = null;

	/**
	 * This is to specify the view mode of VR videos
	 *
	 * @var ViewModeParameter
	 */
	private $viewMode;

	/**
	 * @var string|null One of the ENVIRONMENT_* constants to specify which (mglube) encoder should encode
	 * 		the video, or null to use the default.
	 */
	private $environment = null;
	
	/**
	 * @var string This indicates which encoding service should be used for this job.  It must one of the
	 * ENCODING_SERVICE_* constants.
	 */
	private $encodingService = self::ENCODING_SERVICE_LITMUS;

	/**
	 * Set VR video view mode
	 * @param ViewModeParameter $viewMode
	 * @return $this
	 */
	final public function setViewMode(ViewModeParameter $viewMode) {
		$this->viewMode = $viewMode;
		return $this;
	}

	/**
	 * Return defined viewMode
	 * @return ViewModeParameter
	 */
	final public function getViewMode() {
		return $this->viewMode;
	}

	/**
	 * Sets the conditions for the video to be valid.
	 *
	 * @param ValidConditionsParameter $validConditions
	 * @return self
	 */
	final public function setValidConditions(ValidConditionsParameter $validConditions)
	{
		$this->validConditions = $validConditions;
		return $this;
	}

	/**
	 * Returns the defined valid conditions
	 *
	 * @return ValidConditionsParameter
	 */
	final public function getValidConditions()
	{
		return $this->validConditions;
	}

	/**
	 * Sets the maximum percentage difference in metrics (e.g. resolution) where the metrics are considered identical.
	 *
	 * @param float $duplicateThreshold
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setDuplicateThreshold($duplicateThreshold = 15.0)
	{
		$duplicateThreshold = (float) $duplicateThreshold;

		if ($duplicateThreshold < 0) {
			throw new InvalidParameterException('Duplicate threshold value must be greater than 0.');
		}

		$this->duplicateThreshold = $duplicateThreshold;
		return $this;
	}

	/**
	 * Returns the duplicate threshold
	 *
	 * @return float
	 */
	final public function getDuplicateThreshold()
	{
		return $this->duplicateThreshold;
	}

	/**
	 * Sets auto adjust max video bit rate value.
	 * True if the bit rate is limited proportionally whenever the resolution is limited, false otherwise.
	 *
	 * @param boolean $autoAdjustMaxVideoBitRate
	 * @return self
	 */
	final public function setAutoAdjustMaxVideoBitRate($autoAdjustMaxVideoBitRate = true)
	{
		$this->autoAdjustMaxVideoBitRate = $autoAdjustMaxVideoBitRate;
		return $this;
	}

	/**
	 * Returns the auto adjust max video bit rate value.
	 *
	 * @return boolean
	 */
	final public function getAutoAdjustMaxVideoBitRate()
	{
		return $this->autoAdjustMaxVideoBitRate;
	}

	/**
	 * Sets the url to call back the site after receiving the response from the encoder.
	 *
	 * @param $url
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setCallbackUrl($url)
	{
		if (mb_strpos($url, 'http://') === 0 || mb_strpos($url, 'https://') === 0) {
			$this->callbackUrl = $url;
		} else {
			throw new InvalidParameterException('Invalid callback url format.');
		}
		return $this;
	}

	/**
	 * Returns the call back url.
	 *
	 * @return string
	 */
	final public function getCallbackUrl()
	{
		return $this->callbackUrl;
	}

	/**
	 * Sets the encoder tag
	 * Can be:
	 *    For Production: amsv2, amsv2_gaytube, amsv2_spam, amsv2_wcams, amsv2_xtube, chiV2, chunk_upload
	 *    For Stage: stagev2;
	 *
	 * @param string $encoderTag
	 * @throws \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @return $this
	 */
	final public function setEncoderTag($encoderTag)
	{
		if (null == $encoderTag || '' == $encoderTag) {
			throw new InvalidParameterException('Encoder Tag can\'t be empty.');
		}

		$this->encoderTag = $encoderTag;

		return $this;
	}

	/**
	 * Returns the encoder tag
	 *
	 * @return string
	 */
	final public function getEncoderTag()
	{
		return $this->encoderTag;
	}

	/**
	 * @return string Returns the encoder tag, or the tag of an operation, which overrides the main tag if
	 * it is specified.
	 */
	public function getEffectiveEncoderTag()
	{
		foreach ($this->getOperations() as $operation) {
			if ($operation instanceof Operation) {
				$tag = $operation->getEncoderTag();
				if (false == empty($tag) && Operation::ENCODER_TAG_DEFAULT != $tag) {
					return $tag;
				}
			}
		}
		return $this->getEncoderTag();
	}

	/**
	 * @return VideoMetrics|null @see $originalVideoMetrics
	 */
	public function getOriginalVideoMetrics()
	{
		return $this->originalVideoMetrics;
	}

	/**
	 * @param VideoMetrics|null $originalVideoMetrics @see $originalVideoMetrics
	 */
	public function setOriginalVideoMetrics($originalVideoMetrics)
	{
		$this->originalVideoMetrics = $originalVideoMetrics;
	}

	/**
	 * @return string|null @see $environment
	 */
	public function getEnvironment()
	{
		return $this->environment;
	}

	/**
	 * @param string|null @see $environment
	 * @return $this
	 */
	public function setEnvironment($environment)
	{
		$this->environment = $environment;
		return $this;
	}

	/**
	 * @return string @see $encodingService
	 */
	public function getEncodingService()
	{
		return $this->encodingService;
	}

	/**
	 * @param string $encodingService @see $encodingService
	 * @return $this
	 */
	public function setEncodingService($encodingService)
	{
		$this->encodingService = $encodingService;
		return $this;
	}

}
