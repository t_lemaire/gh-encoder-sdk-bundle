<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\DateTimeInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;

/**
 * Interface JobInterface
 *
 * @package GorillaHub\SDKs\EncodeBundle\V0001\Domain
 */
interface JobInterface extends SDKCallInterface
{

	/**
	 * @param string $id
	 *
	 * @return $this
	 */
	public function setCustomId($id);

	/**
	 * @return string
	 */
	public function getCustomId();

	/**
	 * Returns the operation list
	 *
	 * @return OperationInterface[]
	 */
	public function getOperations();

	/**
	 * Add a single operations to the list
	 *
	 * @param OperationInterface $operation
	 * @return self
	 */
	public function addOperation(OperationInterface $operation);

	/**
	 * Sets an array of operations, overwriting any that are already associated with the job
	 * @param OperationInterface[] $operations
	 * @return self
	 */
	public function setOperations(array $operations);

	/**
	 * Returns the post-operations list
	 *
	 * @return OperationInterface[]
	 */
	public function getPostOperations();

	/**
	 * Add a single post operation to the list
	 *
	 * @param OperationInterface $operation
	 * @return self
	 */
	public function addPostOperation(OperationInterface $operation);

	/**
	 * Sets the post processing operations
	 *
	 * @param OperationInterface[] $postOperations
	 * @return self
	 */
	public function setPostOperations($postOperations);

	/**
	 * Sets the Date Time representation of the job
	 *
	 * @param DateTimeInterface $dateTime
	 *
	 * @return mixed
	 */
	public function setDateTime(DateTimeInterface $dateTime);

	/**
	 * Returns the date time representation of the job
	 *
	 * @return DateTimeInterface
	 */
	public function getDateTime();

}