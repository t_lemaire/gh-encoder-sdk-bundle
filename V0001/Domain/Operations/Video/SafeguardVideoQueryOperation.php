<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;


/**
 * If this operation is present in a job, then Safeguard (an internal MindGeek service) is queried to see if the
 * video is already in their database.  If this process is successful then the client receives a SuccessCall
 * callback with the "result" field set to an instance of SafeguardVideoQueryResult.  Otherwise, the client receives a
 * FailureCall callback for this operation.
 *
 * While this operation is provided for convenience, it is recommended that you use SubscribeToSafeguardVideoOperation
 * instead.  If you use that operation, then if your content is added to the Safeguard database at a later time
 * you will be notified.  See the docblock of SubscribeToSafeguardVideoOperation for details.
 */
class SafeguardVideoQueryOperation extends Operation implements GenerateCallBackInterface, HasNoFilePatternInterface
{

    /**
     * @var int|null An integer between 0 and 100.  The percentage below which audio matches will be considered
     *      noise, or null to use the server-side default.  At the time of this writing, the server-side default is 5.
     */
    private $minAudioMatchPercent;

    /**
     * @var int|null An integer between 0 and 100.  The percentage below which video matches (for the basic video hash
     *      system) will be considered noise, or null to use the server-side default.  At the time of this writing,
     *      the server-side default is 5.
     */
    private $minVideoMatchPercent;

    /**
     * @var string The name of the Safeguard database to check.
     */
    private $source = 'tubes';


    /**
     * @return int|null An integer between 0 and 100.  The percentage below which audio matches will be considered
     *      noise, or null to use the server-side default.  At the time of this writing, the server-side default is 5.
     */
    public function getMinAudioMatchPercent()
    {
        return $this->minAudioMatchPercent;
    }

    /**
     * @param int|null $minAudioMatchPercent An integer between 0 and 100.  The percentage below which audio matches
     *      will be considered noise, or null to use the server-side default.  At the time of this writing, the
     *      server-side default is 5.
     * @return $this
     */
    public function setMinAudioMatchPercent($minAudioMatchPercent)
    {
        $this->minAudioMatchPercent = $minAudioMatchPercent;
        return $this;
    }

    /**
     * @return int|null An integer between 0 and 100.  The percentage below which video matches (for the basic video
     *      hash system) will be considered noise, or null to use the server-side default.  At the time of this
     *      writing, the server-side default is 5.
     */
    public function getMinVideoMatchPercent()
    {
        return $this->minVideoMatchPercent;
    }

    /**
     * @param mixed $minVideoMatchPercent An integer between 0 and 100.  The percentage below which video matches (for
     *      the basic video hash system) will be considered noise, or null to use the server-side default.  At the time
     *      of this writing, the server-side default is 5.
     * @return $this
     */
    public function setMinVideoMatchPercent($minVideoMatchPercent)
    {
        $this->minVideoMatchPercent = $minVideoMatchPercent;
        return $this;
    }

    /**
     * @return string The name of the Safeguard database to check.
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source The name of the Safeguard database to check.
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }
}
