<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;


/**
 * This operation is like DetectCSAIOperation, except for the following differences:
 *
 *      1) The usual SuccessCall will not contain a DetectCSAIResult; it will contain a SubscribeToDetectCSAIResult
 *          instead.  This class (deliberately) does not contain any information; the callback simply tells you that
 *          the fingerprints were generated and that you are subscribed to updates.
 *
 *      2) setFilePattern() _must_ be called with a valid file pattern.  The fingerprint file must be stored on
 *          your Isilon for reuse.  If the fingerprints are deleted, the subscription will be disabled.
 *
 *      3) The Upload Service resubmits the fingerprint to Google periodically.  Each time, the Upload Service forwards
 *          the result to your site's subscription callback URL (which is configured in the upload service database, in
 *          the `litmus`.`clients` table).  In particular, a DetectCSAIUpdateCall is sent to the subscription URL.
 *          Note that this call is different from the usual DetectCSAIResult in that information about the original
 *          call and the original operation are not included.  If you want to identify which video the callback relates
 *          to, you must use the getCustomId() method of the DetectCSAIUpdateCall.  This will return the custom ID that
 *          you specified by calling setCustomId() on the SubscribeToDetectCSAIOperation.
 */
class SubscribeToDetectCSAIOperation extends DetectCSAIOperation
{

    /** @var int|null @see setLastScanTime() */
    private $lastScanTime = null;

    /** @var int|null @see setMediaTime() */
    private $mediaTime = null;

    /** @var bool @see setScanImmediately() */
    private $scanImmediately = false;

    /** @var string|null @see setSubscriptionCallbackUrl() */
    private $subscriptionCallbackUrl = null;

    /** @var int|null @see setExpiryTime() */
    private $expiryTime = null;


    /**
     * @return int|null  @see setLastScanTime()
     */
    public function getLastScanTime()
    {
        return $this->lastScanTime;
    }

    /**
     * @param int|null $lastScanTime The Unix timestamp of the last time the video was scanned, or null if never.
     *      If this is not known, it should at least be estimated.  This time used to decide when the next re-scan
     *      is necessary.
     * @return $this
     */
    public function setLastScanTime($lastScanTime)
    {
        $this->lastScanTime = $lastScanTime;
        return $this;
    }

    /**
     * @return int|null @see setMediaTime()
     */
    public function getMediaTime()
    {
        return $this->mediaTime;
    }

    /**
     * @param int|null $mediaTime The Unix timestamp of the time at which the video was created, or at least uploaded,
     *      or null if not known.  We use this because older content is scanned less frequently.
     * @return $this
     */
    public function setMediaTime($mediaTime)
    {
        $this->mediaTime = $mediaTime;
        return $this;
    }

    /**
     * @return boolean @see setScanImmediately()
     */
    public function getScanImmediately()
    {
        return $this->scanImmediately;
    }

    /**
     * @param boolean $scanImmediately True if the video should be scanned immediately even if a re-scan doesn't
     *      seem to be due.  Set this to true if the site has no CSAI results about this video at all.  Note:  The
     *      results of this immediate scan are sent via the subscription callback URL.
     * @return $this
     */
    public function setScanImmediately($scanImmediately)
    {
        $this->scanImmediately = $scanImmediately;
        return $this;
    }

    /**
     * @return string @see setSubscriptionCallbackUrl()
     */
    public function getSubscriptionCallbackUrl()
    {
        return $this->subscriptionCallbackUrl;
    }

    /**
     * @param string|null $subscriptionCallbackUrl If this is not null, then any future DetectCSAIUpdateCall calls
     *      are sent to this URL.  If this is null, then the site's default subscription callback URL is used.  It
     *      is recommended that this is usually set to null, first to save space in the upload service database, and
     *      second so that it can be changed easily in the future if necessary.
     *      IMPORTANT:  This does NOT change the callback URL of the job, or this operation.  The
     *      SubscribeToDetectCSAIResult is sent to the usual callback URL that is specified for the overall job.  This
     *      only affects where the DetectCSAIUpdateCall is sent, and only for this particular call.
     *
     * @return $this
     */
    public function setSubscriptionCallbackUrl($subscriptionCallbackUrl)
    {
        $this->subscriptionCallbackUrl = $subscriptionCallbackUrl;
        return $this;
    }

    /**
     * @return int|null @see setExpiryTime()
     */
    public function getExpiryTime()
    {
        return $this->expiryTime;
    }

    /**
     * @param int|null $expiryTime The unix timestamp of the time at which this subscription should be deleted, or
     *      null if the subscription should never be deleted.  This feature is typically used for testing.
     *      Production subscriptions should not expire.
     * @return $this
     */
    public function setExpiryTime($expiryTime)
    {
        $this->expiryTime = $expiryTime;
        return $this;
    }



}
