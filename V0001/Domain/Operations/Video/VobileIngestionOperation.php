<?php

namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;

/**
 * This operation causes the video to be registered as a new video in the Vobile database.  You must
 * pass a description of the video to setMetadataXML().  Optionally, you can add the video to the Vobile tracking
 * server by calling setTrackingXML().
 *
 * When this operation is complete, you will get a callback with a VobileIngestionResult:
 * https://stash.mgcorp.co/projects/TUB/repos/services.sdk.bundle/browse/V0001/Domain/Results/Video/VobileIngestionResult.php
 */
class VobileIngestionOperation extends VideoOperation implements GenerateCallBackInterface, HasNoFilePatternInterface
{
    /**
     * @var string The metadata that describes the video.  See:
     *      https://wiki.mgcorp.co/display/ph/Video+Match+Service+-+Vobile+Metadata+XML
     */
    private $metadataXML;

    /**
     * @var string|null The XML with the <TrackingSettings> element describing how this video should be tracked,
     *      or null if the video should not be tracked by the tracking server.
     */
    private $trackerXML = null;

    /**
     * @return string The metadata that describes the video.  See:
     *      https://wiki.mgcorp.co/display/ph/Video+Match+Service+-+Vobile+Metadata+XML
     */
    public function getMetadataXML(): string {
        return $this->metadataXML;
    }

    /**
     * @param string $metadataXML The metadata that describes the video.  See:
     *      https://wiki.mgcorp.co/display/ph/Video+Match+Service+-+Vobile+Metadata+XML
     * @return $this;
     */
    public function setMetadataXML(string $metadataXML) {
        if (!is_string($metadataXML) || simplexml_load_string($metadataXML) === false) {
            throw new \LogicException("Tried passing non-XML as Vobile metadata XML.");
        }
        $this->metadataXML = $metadataXML;
        return $this;
    }

    /**
     * @return string|null The XML with the <TrackingSettings> element describing how this video should be tracked,
     *      or null if the video should not be tracked by the tracking server.
     */
    public function getTrackerXML()
    {
        return $this->trackerXML;
    }

    /**
     * @param string|null $trackerXML The XML with the <TrackingSettings> element describing how this video should be
     *      tracked, or null if the video should not be tracked by the tracking server.
     * @return VobileIngestionOperation
     */
    public function setTrackerXML($trackerXML)
    {
        if ($trackerXML !== null) {
            if (!is_string($trackerXML) || simplexml_load_string($trackerXML) === false) {
                throw new \LogicException("Tried passing non-XML as Vobile tracker XML.");
            }
        }
        $this->trackerXML = $trackerXML;
        return $this;
    }

}