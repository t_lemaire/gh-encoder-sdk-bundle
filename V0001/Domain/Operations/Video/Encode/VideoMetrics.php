<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class VideoMetrics
{

	/**
	 * The width of the video, or null if not specified.
	 *
	 * @var int|null
	 */
	private $width = null;

	/**
	 * The height of the video, or null if not specified.
	 *
	 * @var int|null
	 */
	private $height = null;

	/**
	 * The bit rate, in bits per second, or null if not specified.
	 *
	 * @var int|null
	 */
	private $bitsPerSecond = null;

	/**
	 * The number of frames per second, or null if not specified.
	 *
	 * @var int|null
	 */
	private $framesPerSecond = null;

	/**
	 * Minimum bits per second through
	 *
	 * mandatory for 'vbr' encoding_mode; this specifies the minimum allowable peak video bit rate in a VBR video
	 *
	 * @var int
	 */
	private $minimumBitsPerSecondTrough = null;

	/**
	 * Maximum bits per second peak
	 *
	 * mandatory for 'vbr' encoding_mode; this specifies the maximum allowed instantaneous video bit rate in a VBR video.
	 *
	 * @var int
	 */
	private $maximumBitsPerSecondPeak = null;

	/**
	 * The value should be starting from 'video_bitrate'.it tells how far ffmpeg will adjust the current video bit rate according to 'video_bitrate'.
	 *
	 * @var int
	 */
	private $bufferSize = null;

	/**
	 * It specifies visual video quality ,the number goes higher the quality goes lower.
	 * It only work with "crf" encoding mode. Videolube is also using this parameter too.
	 *
	 * @var integer
	 */
	private $quality = null;

	/**
	 * It specifies how many seconds in between each key frames. It is not recommanded to specify.
	 * The default value will be 3 seconds
	 *
	 * @var integer
	 */
	private $keyframeInt = null;

	/**
	 * Video duration in seconds (either decimal or int)
	 * @var mixed
	 */

	private $duration = null;
	
	/**
	 * Set the bits per second. [200K, 300K, 600K...]
	 *
	 * @param int $bitsPerSecond
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setBitsPerSecond($bitsPerSecond)
	{
		if (null !== $bitsPerSecond) {
			$bitsPerSecond = (int) $bitsPerSecond;

			if ($bitsPerSecond <= 0) {
				throw new InvalidParameterException('Bits per second value must be greater than 0.');
			}
		}

		$this->bitsPerSecond = $bitsPerSecond;
		return $this;
	}

	/**
	 * Returns the bits per second
	 *
	 * @return int|null
	 */
	final public function getBitsPerSecond()
	{
		return $this->bitsPerSecond;
	}

	/**
	 * Set the frames per second (by default 24).
	 *
	 * @param int $framesPerSecond
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setFramesPerSecond($framesPerSecond = 24)
	{
		if (null !== $framesPerSecond) {
			$framesPerSecond = (int) $framesPerSecond;

			if ($framesPerSecond <= 0) {
				throw new InvalidParameterException('Frames per second value must be greater than 0.');
			}
		}

		$this->framesPerSecond = $framesPerSecond;
		return $this;
	}

	/**
	 * Returns the frames per second.
	 *
	 * @return int
	 */
	final public function getFramesPerSecond()
	{
		return $this->framesPerSecond;
	}

	/**
	 * Set the height.
	 *
	 * @param int $height
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setHeight($height)
	{
		if (null !== $height) {
			$height = (int) $height;

			if ($height <= 0) {
				throw new InvalidParameterException('Height value must be greater than 0.');
			}
		}

		$this->height = $height;
		return $this;
	}

	/**
	 * Returns the height.
	 *
	 * @return int|null
	 */
	final public function getHeight()
	{
		return $this->height;
	}

	/**
	 * Set the with.
	 *
	 * @param int $width
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setWidth($width)
	{
		if (null !== $width) {
			$width = (int) $width;

			if ($width <= 0) {
				throw new InvalidParameterException('Width value must be greater than 0.');
			}
		}

		$this->width = $width;
		return $this;
	}

	/**
	 * Returns the width.
	 *
	 * @return int|null
	 */
	final public function getWidth()
	{
		return $this->width;
	}

	/**
	 * Returns the minimum bits per second through
	 *
	 * @return int|null
	 */
	final public function getMinimumBitsPerSecondTrough()
	{
		return $this->minimumBitsPerSecondTrough;
	}

	/**
	 * Sets the minimum bits per second through
	 *
	 * mandatory for 'vbr' encoding_mode; this specifies the minimum allowable peak video bit rate in a VBR video
	 *
	 * @param int $minimumBitsPerSecondTrough
	 *
	 * @return $this
	 */
	final public function setMinimumBitsPerSecondTrough($minimumBitsPerSecondTrough)
	{
		$this->minimumBitsPerSecondTrough = (int) $minimumBitsPerSecondTrough;

		return $this;
	}

	/**
	 * Returns the maximum bits per second peak
	 *
	 * @return int|null
	 */
	final public function getMaximumBitsPerSecondPeak()
	{
		return $this->maximumBitsPerSecondPeak;
	}

	/**
	 * Sets the maximum bits per second peak
	 *
	 * mandatory for 'vbr' encoding_mode; this specifies the maximum allowed instantaneous video bit rate in a VBR video.
	 *
	 * @param int $maximumBitsPerSecondPeak
	 *
	 * @return $this
	 */
	final public function setMaximumBitsPerSecondPeak($maximumBitsPerSecondPeak)
	{
		$this->maximumBitsPerSecondPeak = (int) $maximumBitsPerSecondPeak;

		return $this;
	}

	/**
	 * Returns the buffer size
	 *
	 * @return int|null
	 */
	final public function getBufferSize()
	{
		return $this->bufferSize;
	}

	/**
	 * Sets the bugffer size
	 *
	 * @param null $bufferSize
	 *
	 * @return $this
	 */
	final public function setBufferSize($bufferSize)
	{
		$this->bufferSize = $bufferSize;

		return $this;
	}

	/**
	 * Get the visual video quality
	 * @return int
	 */
	final public function getQuality()
	{
		return $this->quality;
	}

	/**
	 * Set the visual video quality ,the number goes higher the quality goes lower.
	 * It only work with "crf" encoding mode. Videolube is also using this parameter too.
	 *
	 * @param int $quality
	 * @return $this
	 */
	final public function setQuality($quality)
	{
		$this->quality = $quality;

		return $this;
	}

	/**
	 * Get the key frame interval
	 * @return int
	 */
	final public function getKeyframeInt()
	{
		return $this->keyframeInt;
	}

	/**
	 * Set how many seconds in between each key frames. It is not recommanded to specify.
	 * The default value will be 3 seconds
	 *
	 * @param int $keyframeInt
	 * @return $this
	 */
	final public function setKeyframeInt($keyframeInt)
	{
		$this->keyframeInt = $keyframeInt;

		return $this;
	}

	/**
	 * Returns video duration in seconds (either decimal or int)
	 * @return mixed
	 * */
	public function getDuration()
	{
		return $this->duration;
	}

	/** @param mixed $seconds */
	public function setDuration($seconds)
	{
		$this->duration = $seconds;
		return $this;
	}

}