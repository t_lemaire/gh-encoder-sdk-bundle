<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class AudioMetrics
{

	/**
	 * The bit rate, in bits per second, or null if not specified.
	 *
	 * @var int|null
	 */
	private $bitsPerSecond = null;

	/**
	 * The sample rate, in samples per second, or null if not specified.
	 *
	 * @var int|null
	 */
	private $samplesPerSecond = null;

	/**
	 * The number of audio channels, or null if not specified.
	 *
	 * @var int|null
	 */
	private $numberOfChannels = null;

	/**
	 * Set the bit rate [64K, 96K, 128K, 256K...], in bits per second.
	 *
	 * @param int $bitsPerSecond
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setBitsPerSecond($bitsPerSecond)
	{
		if (null !== $bitsPerSecond) {
			$bitsPerSecond = (int) $bitsPerSecond;

			if ($bitsPerSecond <= 0) {
				throw new InvalidParameterException('Bits per second value must be greater than 0.');
			}
		}

		$this->bitsPerSecond = $bitsPerSecond;
		return $this;
	}

	/**
	 * Returns the bit rate in bits per second.
	 *
	 * @return int|null
	 */
	final public function getBitsPerSecond()
	{
		return $this->bitsPerSecond;
	}

	/**
	 * Set the number of channels.
	 *
	 * @param int $numberOfChannels
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setNumberOfChannels($numberOfChannels)
	{
		if (null !== $numberOfChannels) {
			$numberOfChannels = (int) $numberOfChannels;

			if ($numberOfChannels < 0) {
				throw new InvalidParameterException('Number of channels value must be non-negative.');
			}
		}

		$this->numberOfChannels = $numberOfChannels;
		return $this;
	}

	/**
	 * Returns the number of channels.
	 *
	 * @return int|null
	 */
	final public function getNumberOfChannels()
	{
		return $this->numberOfChannels;
	}

	/**
	 * Set the sample rate, in samples per second.
	 *
	 * @param int $samplesPerSecond
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setSamplesPerSecond($samplesPerSecond)
	{
		if (null !== $samplesPerSecond) {
			$samplesPerSecond = (int) $samplesPerSecond;

			if ($samplesPerSecond <= 0) {
				throw new InvalidParameterException('Samples per second value must be greater than 0.');
			}
		}

		$this->samplesPerSecond = $samplesPerSecond;
		return $this;
	}

	/**
	 * Returns the sample rate, in samples per second.
	 *
	 * @return int|null
	 */
	final public function getSamplesPerSecond()
	{
		return $this->samplesPerSecond;
	}

}