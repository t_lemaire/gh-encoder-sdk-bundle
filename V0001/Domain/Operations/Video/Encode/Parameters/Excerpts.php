<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

/**
 * If these parameters are present, then the resulting video should include periodic excerpts from the video instead
 * of the entire video.
 */
class Excerpts
{
	/**
	 * @var int The video is evenly divided into this number of intervals, and one excerpt is taken from each interval.
	 */
	private $numberOfExcerpts;

	/** @var float The maximum duration, in seconds, of each excerpt. */
	private $excerptDuration;

	/**
	 * @var float Any value from 0.0 to 1.0.  0.0 means that each excerpt is taken from the beginning of the
	 * interval, 0.5 the middle, 1.0 the end.
	 */
	private $excerptLocation = 0.5;

	/**
	 * @return int @see $numberOfExcerpts
	 */
	public function getNumberOfExcerpts()
	{
		return $this->numberOfExcerpts;
	}

	/**
	 * @param int $numberOfExcerpts @see $numberOfExcerpts
	 * @return $this
	 */
	public function setNumberOfExcerpts($numberOfExcerpts)
	{
		$this->numberOfExcerpts = $numberOfExcerpts;
		return $this;
	}

	/**
	 * @return float @see $excerptDuration
	 */
	public function getExcerptDuration()
	{
		return $this->excerptDuration;
	}

	/**
	 * @param float $excerptDuration @see $excerptDuration
	 * @return $this
	 */
	public function setExcerptDuration($excerptDuration)
	{
		$this->excerptDuration = $excerptDuration;
		return $this;
	}

	/**
	 * @return float @see $excerptLocation
	 */
	public function getExcerptLocation()
	{
		return $this->excerptLocation;
	}

	/**
	 * @param float $excerptLocation @see $excerptLocation
	 * @return $this
	 */
	public function setExcerptLocation($excerptLocation)
	{
		$this->excerptLocation = $excerptLocation;
		return $this;
	}

    /**
     * @return float The total duration of all requested excerpts.
     */
    public function getTotalDuration()
    {
        return $this->getNumberOfExcerpts() * $this->getExcerptDuration();
    }


}