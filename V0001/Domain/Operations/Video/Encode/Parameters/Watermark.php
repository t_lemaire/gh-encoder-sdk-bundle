<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class Watermark
{
	const TAG_PORNHUB_180P = 'pornhub-180p';
	const TAG_PORNHUB_240P = 'pornhub-240p';
	const TAG_PORNHUB_480P = 'pornhub-480p';
	const TAG_PORNHUB_720P = 'pornhub-720p';
	const TAG_PORNHUB_3GP  = 'pornhub-3gp';

	const POSITION_X_RIGHT = 'right';
	const POSITION_X_CENTER = 'center';
	const POSITION_X_LEFT = 'left';

	/**
	 * The tag of a watermark that is known to the encoder.
	 * Exactly one of $tag and $imageUrl must be non-null.
	 *
	 * @var string|null
	 */
	private $tag = null;

	/**
	 * The URL from which the encoder may download the watermark image (must be a PNG).
	 * Exactly one of $tag and $imageUrl must be non-null.
	 *
	 * @var string|null
	 */
	private $imageUrl = null;

	/**
	 *  This is p/q, where q is the height of the video, and p is the height at which the watermark
	 *    should appear.  For example, a value of 6.0 indicates that the watermark should appear at 1/6th of the
	 *    height of the video.  The watermark is displayed at its normal aspect ratio.  If this parameter is null,
	 *    then the value is taken to be p/q, where q is the height of the video (in pixels) and p is the height
	 *    of the watermark image (in pixels).
	 *
	 * @var float|null
	 */
	private $heightRatio = null;


	private $positionX = self::POSITION_X_RIGHT;

	/**
	 *  This is p/q, where q is the height of the video, and p is the height at which the watermark
	 *    should appear.  For example, a value of 6.0 indicates that the watermark should appear at 1/6th of the
	 *    height of the video.  The watermark is displayed at its normal aspect ratio.  If this parameter is null,
	 *    then the value is taken to be p/q, where q is the height of the video (in pixels) and p is the height
	 *    of the watermark image (in pixels).
	 *
	 * @param float $ratio
	 *
	 * @returns self
	 * @throws InvalidParameterException
	 */
	final public function setHeightRatio($ratio)
	{
		if (null !== $ratio) {
			$ratio = (float)$ratio;

			if ($ratio <= 0) {
				throw new InvalidParameterException('Height Ratio value must be greater than 0.');
			}
		}

		$this->heightRatio = $ratio;

		return $this;
	}

	/**
	 * Returns the Height Ratio.
	 *
	 * @return float|null
	 */
	final public function getHeightRatio()
	{
		return $this->heightRatio;
	}

	/**
	 * Set the URL from which the encoder may download the watermark image (must be a PNG).
	 * Since $imageUrl and $tag must not coexist at the same time, setting the image url will cause $tag to be null.
	 *
	 * @param string $imageUrl
	 *
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setImageUrl($imageUrl)
	{
		if (!is_string($imageUrl)) {
			throw new InvalidParameterException('Image Url has to be a string value.');
		}

		if (mb_strpos($imageUrl, 'http://') === 0 || mb_strpos($imageUrl, 'https://') === 0) {
			$this->imageUrl = $imageUrl;
			$this->tag      = null;
		} else {
			throw new InvalidParameterException(
				"Invalid image url format. The given URL was : '$imageUrl'"
			);
		}

		return $this;
	}

	/**
	 * Returns the Image Url
	 *
	 * @return null|string
	 */
	final public function getImageUrl()
	{
		return $this->imageUrl;
	}

	/**
	 * Set the tag of a watermark that is known to the encoder.
	 * Since $imageUrl and $tag must not coexist at the same time, setting the $tag will cause $imageUrl to be null.
	 *
	 * @param string $tag
	 *
	 * @throws InvalidParameterException
	 * @return self
	 *
	 * @deprecated : please use the setImageUrl() instead
	 */
	final public function setTag($tag)
	{
		if (!is_string($tag)) {
			throw new InvalidParameterException('Tag has to be a string value.');
		}

		$this->tag      = $tag;
		$this->imageUrl = null;

		return $this;
	}

	/**
	 * Returns the tag.
	 *
	 * @return null|string
	 */
	final public function getTag()
	{
		return $this->tag;
	}

	final public function setPositionX($positionX){
		if (!is_string($positionX)) {
			throw new InvalidParameterException('positionX has to be a string value.');
		}

		$value = strtolower($positionX);

		if($value !== self::POSITION_X_LEFT && $value !== self::POSITION_X_RIGHT && $value !== self::POSITION_X_CENTER){
			throw new InvalidParameterException('positionX has to be a valid value.');
		}

		$this->positionX = $value;

		return $this;
	}

	final public function getPositionX(){
		return $this->positionX;
	}

}