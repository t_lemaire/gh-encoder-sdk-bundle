<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

/**
 * Parameters to specify how to encode a VR (virtual reality) video.
 */
class Vr
{
	const SWAP_LEFT_RIGHT = 'left-right';
	const SWAP_TOP_BOTTOM = 'top-bottom';

	/** @var string|null One of the SWAP_* constants, or null if no swapping should be performed. */
	private $swap;

	/**
	 * @return string|null @see $swap
	 */
	public function getSwap()
	{
		return $this->swap;
	}

	/**
	 * @param string|null $swap @see $swap
	 * @return $this
	 * @throws InvalidParameterException if $swap is not a valid option.
	 */
	public function setSwap($swap)
	{
		if (!in_array($swap, array(self::SWAP_LEFT_RIGHT, self::SWAP_TOP_BOTTOM))) {
			throw new InvalidParameterException("Swap must be one of the Vr::SWAP_* constants.");
		}
		$this->swap = $swap;
		return $this;
	}


}