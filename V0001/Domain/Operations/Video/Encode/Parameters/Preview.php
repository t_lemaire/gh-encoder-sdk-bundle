<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class Preview
{

	/**
	 * Duration of the preview, in seconds.
	 *
	 * @var float
	 */
	private $duration;

	/**
	 * The number of seconds into the original video at which the preview begins, or null to let.
	 *
	 * @var int
	 */
	private $startTime = null;

	/**
	 * The duration of the preview to generate if the specified duration and offset are not suitable.
	 *
	 * @var float
	 */
	private $defaultDuration = null;

	/**
	 * Set the default duration of the preview to generate if the specified duration and offset are not suitable.
	 *
	 * @param float|null $duration
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setDefaultDuration($duration)
	{
		if (null !== $duration) {
			$duration = (float) $duration;

			if ($duration <= 0) {
				throw new InvalidParameterException('Default duration value must be greater than 0.');
			}
		}

		$this->defaultDuration = $duration;
		return $this;
	}

	/**
	 * Returns the default duration.
	 *
	 * @return float|null
	 */
	final public function getDefaultDuration()
	{
		return $this->defaultDuration;
	}

	/**
	 * Set the duration of the preview, in seconds.
	 *
	 * @param float $duration
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setDuration($duration)
	{
		$duration = (float) $duration;

		if ($duration <= 0) {
			throw new InvalidParameterException('Duration value must be greater than 0.');
		}

		$this->duration = $duration;
		return $this;
	}

	/**
	 * Returns the preview duration.
	 *
	 * @return float|null
	 */
	final public function getDuration()
	{
		return $this->duration;
	}

	/**
	 * Set the number of seconds into the original video at which the preview begins, or null to let.
	 *
	 * @param float $time
	 * @return self
	 */
	final public function setStartTime($time)
	{
		$this->startTime = (float) $time;

		if ($this->startTime < 0) {
			$this->startTime = 0;
		}
		return $this;
	}

	/**
	 * Returns the start time.
	 *
	 * @return float|null
	 */
	final public function getStartTime()
	{
		return $this->startTime;
	}

}