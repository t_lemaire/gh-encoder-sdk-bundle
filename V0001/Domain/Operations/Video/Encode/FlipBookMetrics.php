<?php

namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode;

use GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class FlipBookMetrics extends ImageMetrics
{
	/**
	 * The number of images to be generated.
	 *
	 * @var int
	 */
	private $number = null;

	/**
	 * Number of second that encoder takes image from video.
	 *
	 * @var float
	 */
	private $interval = null;

	/**
	 * Number of second that encoder take first thumbnail from video.
	 *
	 * @var float
	 */
	private $startOffset = 3.0;

	/**
	 * Prefix name for the output images files.
	 *
	 * @var string
	 */
	private $namePrefix = '';

	/**
	 * If you want to compress the output images.
	 *
	 * @var bool
	 */
	private $compression = true;

	/**
	 * Enable the images compression.
	 *
	 * @param bool $value
	 *
	 * @return $this
	 */
	final public function setCompression($value = true)
	{
		$this->compression = $value;

		return $this;
	}

	/**
	 * Returns true if compression is enable, otherwise false.
	 *
	 * @return bool
	 */
	final public function getCompression()
	{
		return $this->compression;
	}

	/**
	 * Number of second that encoder takes image from video.
	 *
	 * @param float|null $interval
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setInterval($interval)
	{
		if ($this->number > 0) {
			throw new InvalidParameterException('Unable to set the Interval value, a Number value has been defined already, please make sure to use only one of Number or Interval definitions.');
		}

		if (null !== $interval) {
			$interval = (float) $interval;

			if ($interval <= 0) {
				throw new InvalidParameterException('Interval value has to be greater than 0.');
			}
		}

		$this->interval = $interval;
		return $this;
	}

	/**
	 * Returns the number of second that encoder takes image from video.
	 *
	 * @return float
	 */
	final public function getInterval()
	{
		return $this->interval;
	}

	/**
	 * Set the prefix name for images files.
	 *
	 * @param string $namePrefix
	 * @return self
	 */
	final public function setNamePrefix($namePrefix)
	{
		if (!is_string($namePrefix)) {
			$namePrefix = '';
		}

		$this->namePrefix = $namePrefix;
		return $this;
	}

	/**
	 * Returns the prefix name for images files.
	 *
	 * @return string
	 */
	final public function getNamePrefix()
	{
		return $this->namePrefix;
	}

	/**
	 * Sets the number of images to output.
	 *
	 * @param int $number
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setNumber($number)
	{
		if ($this->interval > 0) {
			throw new InvalidParameterException('Unable to set the Number value, an Interval value has been defined already, please make sure to use only one of Number or Interval definitions.');
		}

		if (null !== $number) {
			$number = (int) $number;

			if ($number <= 0) {
				throw new InvalidParameterException('Number value has to be greater than 0.');
			}
		}

		$this->number = $number;
		return $this;
	}

	/**
	 * Returns the number of output images
	 *
	 * @return int
	 */
	final public function getNumber()
	{
		return $this->number;
	}

	/**
	 * Sets the number of second that encoder take first thumbnail from video.
	 *
	 * @param float $startOffset
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setStartOffset($startOffset = 3.0)
	{
		if (null !== $startOffset) {
			$startOffset = (float) $startOffset;

			if ($startOffset < 0) {
				$startOffset = 0;
			}
		}

		$this->startOffset = $startOffset;
		return $this;
	}

	/**
	 * Returns the number of second that encoder take first thumbnail from video.
	 *
	 * @return float
	 */
	final public function getStartOffset()
	{
		return $this->startOffset;
	}
} 