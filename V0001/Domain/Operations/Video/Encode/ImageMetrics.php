<?php

namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Formats\ImageFormats;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePatternInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class ImageMetrics implements FilePatternInterface
{

	/**
	 * Format of the output image.
	 *
	 * @var string
	 */
	private $format = ImageFormats::FORMAT_JPG;

	/**
	 * Width of the output image.
	 *
	 * @var int|null
	 */
	private $width;

	/**
	 * Height of the output image.
	 *
	 * @var int|null
	 */
	private $height;

	/**
	 * Quality of the output image.
	 *
	 * @var int
	 */
	private $quality = 85;

	/**
	 * @var FilePattern
	 */
	private $filePattern;

	/**
	 * @var string
	 */
	private $metricId = '';

	/**
	 * Sets the format of the output image, $format can be jpg, png, gif
	 *
	 * @param string $format
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setFormat($format)
	{
		if ($format !== ImageFormats::FORMAT_JPG && $format !== ImageFormats::FORMAT_PNG && $format !== ImageFormats::FORMAT_GIF) {
			throw new InvalidParameterException('Invalid image format, allowed inputs: jpg, png, gif.');
		}

		$this->format = $format;

		return $this;
	}

	/**
	 * Returns the image output format.
	 *
	 * @return string
	 */
	final public function getFormat()
	{
		return $this->format;
	}

	/**
	 * Sets the output image width. A width value of null means the value is missing or unknown.
     * For flip book operations, a null value means to use the original width.
	 *
	 * @param int|null $width
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setWidth($width)
	{
	    if(!is_null($width)) {
            $width = (int) $width;
            if ($width <= 0) {
                throw new InvalidParameterException('Width value has to be greater than 0 or null.');
            }
        }
		$this->width = $width;
		return $this;
	}

	/**
	 * Returns the output image width.
	 *
	 * @return int|null
	 */
	final public function getWidth()
	{
		return $this->width;
	}

	/**
	 * Sets the output image height. A height value of null means the value is missing or unknown.
     * For flip book operations, a null value means to use the original height.
	 *
	 * @param int|null $height
	 * @throws InvalidParameterException
	 * @return self
	 */
	final public function setHeight($height)
	{
	    if(!is_null($height)) {
            $height = (int) $height;
            if ($height <= 0) {
                throw new InvalidParameterException('Height value has to be greater than 0 or null.');
            }
        }
		$this->height = $height;
		return $this;
	}

	/**
	 * Returns the output image height.
	 *
	 * @return int|null
	 */
	final public function getHeight()
	{
		return $this->height;
	}

	/**
	 * Sets the image output quality.
	 *
	 * @param int $quality
	 * @return self
	 */
	final public function setQuality($quality = 85)
	{
		$quality = (int) $quality;

		if ($quality < 0) {
			$quality = 0;
		}

		if ($quality > 100) {
			$quality = 100;
		}

		$this->quality = $quality;

		return $this;
	}

	/**
	 * Returns the image output quality.
	 *
	 * @return int
	 */
	final public function getQuality()
	{
		return $this->quality;
	}

	/**
	 * @param FilePattern $filePattern
	 *
	 * @return $this
	 */
	final public function setFilePattern(FilePattern $filePattern)
	{
		$this->filePattern = $filePattern;

		return $this;
	}

	/**
	 * @return FilePattern
	 */
	final public function getFilePattern()
	{
		return $this->filePattern;
	}

	/**
	 * @param string $metricId
	 *
	 * @return $this
	 */
	final public function setMetricId($metricId)
	{
		$this->metricId = $metricId;

		return $this;
	}

	/**
	 * @return string
	 */
	final public function getMetricId()
	{
		return $this->metricId;
	}

}