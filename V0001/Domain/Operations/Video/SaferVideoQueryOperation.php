<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\SaferQueryOperation;

/**
 * If this operation is present in a job, then Safer (an external service) is queried to see if the video is already
 * in their database.  If this process is successful then the client receives a SuccessCall callback with the "result"
 * field set to an instance of SaferVideoQueryResult.  Otherwise, the client receives a FailureCall callback for this
 * operation.
 */
class SaferVideoQueryOperation extends SaferQueryOperation
{

}
