<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\ImageBasedOperation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;

class FlipBookOperation extends ImageBasedOperation implements GenerateCallBackInterface
{

}