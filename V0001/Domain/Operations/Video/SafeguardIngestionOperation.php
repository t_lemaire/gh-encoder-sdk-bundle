<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;



/**
 * If this operation is present in a job, then the image or video is added to the Safeguard database.  You can use
 * SafeguardQueryOperation to test whether an image or video is similar to an image or video that's already in the
 * Safeguard database.  If this process is successful then the client receives a SuccessCall callback with the "result"
 * field set to an instance of SafeguardIngestionResult.  Otherwise, the client receives a FailureCall callback for
 * this operation.
 */
class SafeguardIngestionOperation extends VideoOperation implements GenerateCallBackInterface, HasNoFilePatternInterface
{
    const TYPE_IMAGE = "image";
    const TYPE_VIDEO = "video";

    /**
     * @var string One of the TYPE_* constants.
     */
    private $type;

    /**
     * @var string The ID of this image or video.  This is returned in future query results if this content is
     *      matched.
     */
    private $contentId;

    /**
     * @var string The name of the Safeguard database into which to store the image or video.
     */
    private $source = 'tubes';

    /**
     * @var object|null An object that is recorded alongside the image or video.  This object is passed back as-is
     *      when a future image or video matches this image or video, so it can be used to identify this image/video.
     */
    private $passthrough = null;


    /**
     * @return string|null One of the TYPE_* constants.
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string|null $type One of the TYPE_* constants.
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string The ID of this image or video.  This is returned in future query results if this content is
     *      matched.
     */
    public function getContentId()
    {
        return $this->contentId;
    }

    /**
     * @param string $contentId The ID of this image or video.  This is returned in future query results if this
     *      content is matched.
     * @return $this
     */
    public function setContentId($contentId)
    {
        $this->contentId = $contentId;
        return $this;
    }

    /**
     * @return string The name of the Safeguard database into which to store the image or video.
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source The name of the Safeguard database into which to store the image or video.
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }



    /**
     * @return object|null An object that is recorded alongside the image or video.  This object is passed back as-is
     *      when a future image or video matches this image or video, so it can be used to identify this image/video.
     */
    public function getPassthrough()
    {
        return $this->passthrough;
    }

    /**
     * @param null|object $passthrough An object that is recorded alongside the image or video.  This object is passed
     *      back as-is when a future image or video matches this image or video, so it can be used to identify this
     *      image/video.
     */
    public function setPassthrough($passthrough)
    {
        $this->passthrough = $passthrough;
    }

}
