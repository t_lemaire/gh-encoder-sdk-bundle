<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\NCMECQueryOperation;


/**
 * If this operation is present in a job, then the video is compared to the NCMEC database.  If this process is
 * successful then the client receives a SuccessCall callback with the "result" field set to an instance of
 * NCMECVideoQueryResult.  Otherwise, the client receives a FailureCall callback for this operation.
 */
class NCMECVideoQueryOperation extends NCMECQueryOperation
{

}
