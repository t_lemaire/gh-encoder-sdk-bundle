<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;



/**
 * This operation causes the video to be sent to a CSAI detection service (currently Google) to determine if the
 * video contains any CSAI (child sexual abuse imagery).  If this process is successful then the client receives
 * a SuccessCall callback with the "result" field set to an instance of DetectCSAIResult.  Otherwise, the client
 * receives a FailureCall callback for this operation.
 *
 * This operation generates a fingerprint of the video; a file about 5% the size of the original video.  Since
 * the CSAI detection service does not send us updates if a video is registered to its database _after_ we check,
 * we should periodically recheck videos.  The most expensive part of this procedure is generating the fingerprint.
 * Therefore, we should save the fingerprint somewhere on the Isilon for future use.  If you wish to do this,
 * call setFilePattern() (documented below) to tell the Upload Service where to store the fingerprint files after
 * they are generated.  If you wish to reuse existing fingerprint files, if they are already there, call
 * setReuseFingerprint(true).  Otherwise the Upload Service will always regenerate the fingerprint.
 *
 * The pattern that you pass to setFilePattern() specifies where to store the fingerprint file, or null if the
 * fingerprint file should not be stored (default).  Note that there may be more than one fingerprint file.  In
 * this case, if there is no %i in the file pattern, then the second, third, etc... files will have
 * "-2", "-3", etc... appended before the last period in the name, or to the end of the filename if there
 * are no periods.  (For example, if the requested filename is fingerprint.fp, then the files fingerprint.fp,
 * fingerprint-2.fp, fingerprint-3.fp will be generated.)  It is recommended to use %i somewhere in the filename
 * so that the first file has the number 1 in it, to suggest to anyone stumbling on the files in the future that
 * there _could_ be multiple fingerprints.
 */
class DetectCSAIOperation extends VideoOperation implements GenerateCallBackInterface, HasNoFilePatternInterface
{

    /** @var bool */
    private $reuseFingerprint = false;

	/** @var bool */
	private $forceMatchForTesting = false;

    /**
     * @return boolean @see setReuseFingerprint()
     */
    public function getReuseFingerprint()
    {
        return $this->reuseFingerprint;
    }

    /**
     * @param boolean $reuseFingerprint If this is true, then the upload service will not generate new fingerprints
     *      if there are any files already matching the path specified in $filePattern.
     * @return $this
     */
    public function setReuseFingerprint($reuseFingerprint)
    {
        $this->reuseFingerprint = $reuseFingerprint;
        return $this;
    }

	/**
	 * @return boolean  @see setForceMatchForTesting()
	 */
	public function getForceMatchForTesting()
	{
		return $this->forceMatchForTesting;
	}

	/**
	 * @param boolean $forceMatchForTesting If this is true, then the operation will always match.  This can be used
	 * 		for testing.
	 * @return $this
	 */
	public function setForceMatchForTesting($forceMatchForTesting)
	{
		$this->forceMatchForTesting = $forceMatchForTesting;
		return $this;
	}





}
