<?php

namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;

use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;

class VobileOperation extends VideoOperation implements GenerateCallBackInterface, HasNoFilePatternInterface
{

}