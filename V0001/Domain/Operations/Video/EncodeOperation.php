<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Excerpts;
use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Vr;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\VideoOperation;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\AudioMetrics;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\VideoMetrics;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Preview;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\Parameters\Watermark;

class EncodeOperation extends VideoOperation implements GenerateCallBackInterface
{
	const ENCODING_MODE_1PASS = 'pass';
	const ENCODING_MODE_1PASS_CRF = '1pass-crf';
	const ENCODING_MODE_2PASS = '2pass';
	const ENCODING_MODE_VBR = 'vbr';

	const PROFILE_BASELINE = 'baseline';
	const PROFILE_MAIN = 'main';
	const PROFILE_HIGH = 'high';
	const PROFILE_HIGH_422P = 'high422p';
	const PROFILE_NONE = null;

	const AUDIO_CODEC_COPY = 'copy';
	
	const VIDEO_CODEC_H264 = 'h264';
	const VIDEO_CODEC_H265 = 'h265';

	const ENCODER_MINDGEEK = 'MindGeek';
	const ENCODER_VIDEOLUBE = 'VideoLube';

	/**
	 * The video will be made to fit the requested rectangle by cropping the edges and zooming to fill the space.
	 * The resulting video will be the requested size.
	 */
	const FIT_BY_ZOOMING_AND_CROPPING = 'zoom_and_crop';

	/**
	 * The video will be made to fit the requested rectangle by shrinking it until all of it fits in the space.
	*/
	const FIT_BY_SHRINKING = 'shrink';

	/** The encoder will use a default algorithm (choose this for backward compatibility). */
	const FIT_BY_DEFAULT_METHOD = 'default';

	/**
	 * The video format, e.g. flv, mp4, 3gp.
	 *
	 * @var string
	 */
	private $format;

	/**
	 * The encoding mode.  Possible values are '1pass', '1pass-crf', '2pass', 'vbr'
	 *
	 * @deprecated
	 * @var string
	 */
	private $encodingMode = self::ENCODING_MODE_1PASS;

	/**
	 *    The maximum allowed metrics (e.g. width, height, bit rate) of the
	 *    output video.  The metrics of the output video will be the same as the corresponding metrics of the
	 *    original video, but they will limited so that they do not exceed (by much) any metrics specified in this
	 *    parameter.
	 *
	 *    The size of the output video is chosen so that the aspect ratio is the same as the original video, but
	 *    neither the width nor the height will exceed the maximum width or height specified in this parameter, if
	 *    any.  The encoder does not add borders or clip a portion of the video.
	 *
	 *    Any metric of the resulting video may be less than the specified limit, or it may be slightly greater
	 *    if it must be adjusted to the nearest legal value.
	 *
	 * @var VideoMetrics
	 */
	private $videoLimits;

	/**
	 * The maximum allowed metrics (e.g. sample rate, bit rate) of the
	 * output video.  The metrics of the output video will be the same as the corresponding metrics of the
	 * original video, but they will limited so that they do not exceed (by much) any metrics specified in this
	 * parameter.
	 *
	 * @var AudioMetrics
	 */
	private $audioLimits;

	/**
	 * One of the AUDIO_CODEC_* constants, or null to use the recommended default.
	 *
	 * @var string|null
	 */
	private $audioCodec = null;

	/**
	 * One of the VIDEO_CODEC_* constants, or null to use the recommended default.
	 *
	 * @var string|null
	 */
	private $videoCodec = null;

	/**
	 * The requested preview, or null if none.
	 *
	 * @var Preview|null
	 */
	private $preview = null;

	/**
	 * A tag that is known to the server that identifies the requested intro video, or null if none.
	 *
	 * @var string|null
	 */
	private $introTag = null;

	/**
	 * A tag that is known to the server that identifies the requested outro video, or null if none.
	 *
	 * @var string|null
	 */
	private $outroTag = null;

	/**
	 * The requested watermark, or null if none.
	 *
	 * @var Watermark|null
	 */
	private $watermark = null;

	/**
	 * A prefix that is prepended to the resulting filename.
	 *
	 * @var string
	 */
	private $namePrefix = '';

	/**
	 * A suffix that is appended to the resulting filename.
	 *
	 * @var string
	 */
	private $nameSuffix = '';

	/**
	 * True if the entire job should fail if this video cannot be generated, false if the result may
	 *
	 * @var bool
	 */
	private $mandatory = false;

	/**
	 * Luma coefficient: luma less than 1.0 will darken the video, luma greater than 1.0 will brighten the video.
	 *
	 * @var float
	 */
	private $luma = 1.0;

	/**
	 * Encoding profile. One of 'baseline', 'main', 'high', 'high422p'. Omit this to let the encoder choose a profile.
	 *
	 * @var string
	 */
	private $profile = self::PROFILE_NONE;

	/**
	 * Defines if the video will have a variable bit rate or not (constant bit rate).
	 *
	 * @var boolean
	 */
	private $variableBitRate = null;

	/**
	 * The number of encoding passes, 1 or 2.  2 passes allows the file size to be more predictable (which is irrelevant for streaming).  Default is 1
	 *
	 * @var integer
	 */
	private $numberOfPasses = null;

	/** @var Vr|null */
	private $vrParams = null;

	/** @var Excerpts|null */
	private $excerpts = null;

	/**
	 * @var int[][]|null If this is not null, it is a series of intervals to take from the original video.  Each
	 *		interval is of the form [x,y], where x is the start time (in seconds) and y is the end time, or null
	 * 		to go to the end of the video.
	 */
	private $intervals = null;

	/**
	 * @var string[]|null If this is not null, it is a series of encoding engines to try to use, in descending order
	 * 		of preference.  Elements must each be one of the EncodeOperation::ENCODER_* constants.  If this field is
	 * 		null, the upload service guesses the appropriate encoding engines from the encoding tag.
	 */
	private $encoders = null;

	/**
	 * @var string One of the EncodeOperation::FIT_BY_* constants.  This specifies what action to take if both
	 * 		width and height are requested (meaning that an aspect ratio is explicitly requested) but the source
	 *		video has a taller aspect ratio.
	 */
	private $methodToFitTallVideos = EncodeOperation::FIT_BY_DEFAULT_METHOD;

	/**
	 * @var string One of the EncodeOperation::FIT_BY_* constants.  This specifies what action to take if both
	 * 		width and height are requested (meaning that an aspect ratio is explicitly requested) but the source
	 *		video has a wider aspect ratio.
	 */
	private $methodToFitWideVideos = EncodeOperation::FIT_BY_DEFAULT_METHOD;

	/**
	 * Set the desired audio codec.
	 *
	 * @param string|null $codec One of the AUDIO_CODEC_* constants, or null to use the recommended default.
	 *
	 * @return EncodeOperation
	 */
	final public function setAudioCodec($codec)
	{
		if (null === $codec) {
			$this->audioCodec = null;
		} else {
			$this->audioCodec = self::AUDIO_CODEC_COPY;
		}

		return $this;
	}

	/**
	 * Returns the current audio codec.
	 *
	 * @return null|string
	 */
	final public function getAudioCodec()
	{
		return $this->audioCodec;
	}
	
	
	/**
	 * Sets the desired video codec.
	 *
	 * @param string|null $codec One of the VIDEO_CODEC_* constants, or null to use the recommended default.
	 * @return EncodeOperation
	 * @throws InvalidParameterException
	 */
	final public function setVideoCodec($codec)
	{
		$allowed = [
			self::VIDEO_CODEC_H264,
			self::VIDEO_CODEC_H265
		];
		if ($codec !== null && !in_array($codec, $allowed)) {
			throw new InvalidParameterException(
				'Invalid video codec value, allowed values are: ' . implode(', ', $allowed)
			);
		}
		$this->videoCodec = $codec;
		return $this;
	}

	/**
	 * Returns the current video codec.
	 *
	 * @return null|string One of the VIDEO_CODEC_* constants, or null to use the recommended default.
	 */
	final public function getVideoCodec()
	{
		return $this->videoCodec;
	}	
	
	

	/**
	 * The maximum allowed metrics (e.g. sample rate, bit rate) of the
	 * output video.  The metrics of the output video will be the same as the corresponding metrics of the
	 * original video, but they will limited so that they do not exceed (by much) any metrics specified in this
	 * parameter.
	 *
	 * @param AudioMetrics $audioMetrics
	 *
	 * @return EncodeOperation
	 */
	final public function setAudioLimits(AudioMetrics $audioMetrics)
	{
		$this->audioLimits = $audioMetrics;

		return $this;
	}

	/**
	 * Returns the audio limits.
	 *
	 * @return AudioMetrics
	 */
	final public function getAudioLimits()
	{
		return $this->audioLimits;
	}

	/**
	 * Set the video encoding mode,  Possible values are '1pass', '1pass-crf', '2pass', 'vbr'.
	 *
	 * @deprecated
	 *
	 * @param string $encodingMode
	 *
	 * @throws InvalidParameterException
	 * @return EncodeOperation
	 */
	final public function setEncodingMode($encodingMode)
	{
		if ($encodingMode != self::ENCODING_MODE_1PASS && $encodingMode != self::ENCODING_MODE_1PASS_CRF && $encodingMode != self::ENCODING_MODE_2PASS && $encodingMode != self::ENCODING_MODE_VBR) {
			throw new InvalidParameterException('Invalid encoding mode value, allowed values are: 1pass, 1pass-crf, 2pass, vbr');
		}

		$this->encodingMode = $encodingMode;

		return $this;
	}

	/**
	 * Returns the encoding mode, Possible values are '1pass', '1pass-crf', '2pass', 'vbr'.
	 *
	 * @deprecated
	 * @return string
	 */
	final public function getEncodingMode()
	{
		return $this->encodingMode;
	}

	/**
	 * Set the format of the output video, e.g. flv, mp4, 3gp.
	 *
	 * @param string $format
	 *
	 * @throws InvalidParameterException
	 * @return EncodeOperation
	 */
	final public function setFormat($format)
	{
		if (!is_string($format)) {
			throw new InvalidParameterException('Format has to be a string value.');
		}

		$this->format = $format;

		return $this;
	}

	/**
	 * Returns the format.
	 *
	 * @return string
	 */
	final public function getFormat()
	{
		return $this->format;
	}

	/**
	 * A tag that is known to the server that identifies the requested intro video, or null if none.
	 *
	 * @param null|string $introTag
	 *
	 * @return EncodeOperation
	 */
	final public function setIntroTag($introTag)
	{
		$this->introTag = $introTag;

		return $this;
	}

	/**
	 * Returns the intro tag.
	 *
	 * @return null|string
	 */
	final public function getIntroTag()
	{
		return $this->introTag;
	}

	/**
	 *  A tag that is known to the server that identifies the requested outro video, or null if none.
	 *
	 * @param null|string $outroTag
	 *
	 * @return EncodeOperation
	 */
	final public function setOutroTag($outroTag)
	{
		$this->outroTag = $outroTag;

		return $this;
	}

	/**
	 * Returns the outro tag.
	 *
	 * @return null|string
	 */
	final public function getOutroTag()
	{
		return $this->outroTag;
	}

	/**
	 * Defined only for backward compatibilities.
	 *
	 * @deprecated
	 * @return false
	 */
	final public function getMandatory()
	{
		return $this->mandatory;
	}

	/**
	 * A prefix that is prepended to the resulting filename.
	 *
	 * @param string $namePrefix
	 *
	 * @return EncodeOperation
	 */
	final public function setNamePrefix($namePrefix)
	{
		if (!is_string($namePrefix)) {
			$namePrefix = '';
		}

		$this->namePrefix = $namePrefix;

		return $this;
	}

	/**
	 * Returns the name prefix.
	 *
	 * @return string
	 */
	final public function getNamePrefix()
	{
		return $this->namePrefix;
	}

	/**
	 * A suffix that is appended to the resulting filename.
	 *
	 * @param string $nameSuffix
	 *
	 * @return EncodeOperation
	 */
	final public function setNameSuffix($nameSuffix)
	{
		if (!is_string($nameSuffix)) {
			$nameSuffix = '';
		}

		$this->nameSuffix = $nameSuffix;

		return $this;
	}

	/**
	 * Returns the name suffix.
	 *
	 * @return string
	 */
	final public function getNameSuffix()
	{
		return $this->nameSuffix;
	}

	/**
	 * The requested preview, or null if none.
	 *
	 * @param Preview|null $preview
	 *
	 * @return EncodeOperation
	 */
	final public function setPreview($preview)
	{
		$this->preview = $preview;

		return $this;
	}

	/**
	 * Returns the preview.
	 *
	 * @return Preview|null
	 */
	final public function getPreview()
	{
		return $this->preview;
	}

	/**
	 * The maximum allowed metrics (e.g. width, height, bit rate) of the
	 * output video.  The metrics of the output video will be the same as the corresponding metrics of the
	 * original video, but they will limited so that they do not exceed (by much) any metrics specified in this
	 * parameter.
	 *
	 * The size of the output video is chosen so that the aspect ratio is the same as the original video, but
	 * neither the width nor the height will exceed the maximum width or height specified in this parameter, if
	 * any.  The encoder does not add borders or clip a portion of the video.
	 *
	 * Any metric of the resulting video may be less than the specified limit, or it may be slightly greater
	 * if it must be adjusted to the nearest legal value.
	 *
	 * @param VideoMetrics $videoMetrics
	 *
	 * @return EncodeOperation
	 */
	final public function setVideoLimits(VideoMetrics $videoMetrics)
	{
		$this->videoLimits = $videoMetrics;

		return $this;
	}

	/**
	 * Returns the video metrics.
	 *
	 * @return VideoMetrics
	 */
	final public function getVideoLimits()
	{
		return $this->videoLimits;
	}

	/**
	 * The requested watermark, or null if none.
	 *
	 * @param Watermark|null $watermark
	 *
	 * @return EncodeOperation
	 * @throws InvalidParameterException
	 */
	final public function setWatermark($watermark)
	{
		if ($watermark instanceof Watermark) {
			$tag      = $watermark->getTag();
			$imageUrl = $watermark->getImageUrl();

			if (null === $tag && null === $imageUrl) {
				throw new InvalidParameterException('Watermark must contain exactly one of tag and imageUrl defined.');
			}
		}

		$this->watermark = $watermark;

		return $this;
	}

	/**
	 * Returns the watermark
	 *
	 * @return Watermark|null
	 */
	final public function getWatermark()
	{
		return $this->watermark;
	}

	/**
	 * Sets the luma coefficient
	 * Luma less than 1.0 will darken the video, luma greater than 1.0 will brighten the video.
	 *
	 * @param float $luma
	 *
	 * @return $this
	 */
	final public function setLuma($luma)
	{
		$this->luma = $luma;

		return $this;
	}

	/**
	 * Returns the luma coefficient
	 *
	 * @return float
	 */
	final public function getLuma()
	{
		return $this->luma;
	}

	/**
	 * Sets the encoding profile
	 * One of 'baseline', 'main', 'high', 'high422p'. Omit this to let the encoder choose a profile.
	 *
	 * @param string|null $profile
	 *
	 * @return $this
	 */
	final public function setProfile($profile)
	{
		switch ($profile) {
			case self::PROFILE_BASELINE:
			case self::PROFILE_HIGH:
			case self::PROFILE_HIGH_422P:
			case self::PROFILE_MAIN:
			case self::PROFILE_NONE:
				$this->profile = $profile;
				break;
			default:
				$this->profile = self::PROFILE_NONE;
				break;
		}

		return $this;
	}

	/**
	 * Returns the encoding profile.
	 *
	 * @return string
	 */
	final public function getProfile()
	{
		return $this->profile;
	}

	/**
	 * Returns if the operation has a variable bit rate or not (constant bit rate).
	 *
	 * @return boolean|null
	 */
	final public function getVariableBitRate()
	{
		return $this->variableBitRate;
	}

	/**
	 * Defines if the video will have a variable bit rate or not (constant bit rate).
	 *
	 * @param boolean $variableBitRate
	 *
	 * @return $this
	 */
	final public function setVariableBitRate($variableBitRate = true)
	{
		if ($variableBitRate !== true) {
			$variableBitRate = false;
		}

		$this->variableBitRate = $variableBitRate;

		return $this;
	}

	/**
	 * Return the number of passes
	 *
	 * @return int|null
	 */
	final public function getNumberOfPasses()
	{
		return $this->numberOfPasses;
	}

	/**
	 * Defines the number of encoding passes, 1 or 2.  2 passes allows the file size to be more predictable (which is irrelevant for streaming).  Default is 1
	 *
	 * @param int $numberOfPasses
	 *
	 * @return $this
	 */
	final public function setNumberOfPasses($numberOfPasses)
	{
		$this->numberOfPasses = (int)$numberOfPasses;

		if ($this->numberOfPasses < 0) {
			$this->numberOfPasses = 1;
		}

		return $this;
	}

	/**
	 * @return Vr|null @see $vrParams
	 */
	public function getVrParams()
	{
		return $this->vrParams;
	}

	/**
	 * @param Vr|null $vrParams @see $vrParams
	 * @return $this
	 */
	public function setVrParams($vrParams)
	{
		$this->vrParams = $vrParams;
		return $this;
	}

	/**
	 * @return Excerpts|null @see $excerpts
	 */
	public function getExcerpts()
	{
		return $this->excerpts;
	}

	/**
	 * @param Excerpts|null $excerpts @see $excerpts
	 * @return $this
	 */
	public function setExcerpts($excerpts)
	{
		$this->excerpts = $excerpts;
		return $this;
	}

	/**
	 * @return int[][]|null @see $intervals
	 */
	public function getIntervals()
	{
		return $this->intervals;
	}

	/**
	 * @param int[][]|null $intervals @see $intervals
	 * @return $this
	 */
	public function setIntervals($intervals)
	{
		$this->intervals = $intervals;
		return $this;
	}

	/**
	 * @return null|string[] @see $encoders
	 */
	public function getEncoders()
	{
		return $this->encoders;
	}

	/**
	 * @param null|string[] $encoders @see $encoders
	 * @return $this
	 */
	public function setEncoders($encoders)
	{
		$this->encoders = $encoders;
		return $this;
	}

	/**
	 * @return string @see $methodToFitTallVideos
	 */
	public function getMethodToFitTallVideos()
	{
		return $this->methodToFitTallVideos;
	}

	/**
	 * @param string $methodToFitTallVideos @see $methodToFitTallVideos
	 * @return $this
	 */
	public function setMethodToFitTallVideos($methodToFitTallVideos)
	{
		$this->methodToFitTallVideos = $methodToFitTallVideos;
		return $this;
	}

	/**
	 * @return string @see $methodToFitWideVideos
	 */
	public function getMethodToFitWideVideos()
	{
		return $this->methodToFitWideVideos;
	}

	/**
	 * @param string $methodToFitWideVideos @see $methodToFitWideVideos
	 * @return $this
	 */
	public function setMethodToFitWideVideos($methodToFitWideVideos)
	{
		$this->methodToFitWideVideos = $methodToFitWideVideos;
		return $this;
	}


}
