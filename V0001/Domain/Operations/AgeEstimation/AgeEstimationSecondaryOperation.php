<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\AgeEstimation;


/**
 * If an operation of this type is present in a job, then the image or video is sent to the age estimation service:
 *
 * https://wiki.mgcorp.co/display/DS/DICKS+V2%3A+Tubes
 *
 * The specified pipelines are executed with low priority, and should be used for ALL checks where an end user is
 * not waiting for a result.  If this process is successful then the client receives a SuccessCall callback with the
 * "result" field set to an instance of AgeEstimationResult.  Otherwise, the client receives a FailureCall callback
 * for this operation.
 */
class AgeEstimationSecondaryOperation extends AgeEstimationOperation
{

}
