<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\AgeEstimation;


/**
 * If an operation of this type is present in a job, then the image or video is sent to the age estimation service:
 *
 * https://wiki.mgcorp.co/display/DS/DICKS+V2%3A+Tubes
 *
 * The specified pipelines are executed with high priority, and should be used ONLY for checks that must be done
 * before a new upload can be displayed.  If this process is successful then the client receives a SuccessCall
 * callback with the "result" field set to an instance of AgeEstimationResult.  Otherwise, the client receives a
 * FailureCall callback for this operation.
 */
class AgeEstimationPriorityOperation extends AgeEstimationOperation
{

}
