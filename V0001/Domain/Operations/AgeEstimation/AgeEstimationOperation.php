<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\AgeEstimation;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;


abstract class AgeEstimationOperation extends Operation implements GenerateCallBackInterface, HasNoFilePatternInterface
{
    const ORIENTATION_STRAIGHT = 'straight';
    const ORIENTATION_GAY = 'gay';
    const ORIENTATION_LESBIAN = 'lesbian';
    const ORIENTATION_BISEXUAL = 'bisexual';
    const ORIENTATION_TRANSGENDER = 'transgender';

    /** @var string detects and embeds faces in video */
    const PIPELINE_FACE = 'face';

    /** @var string predicts the age of face embeddings as over 25 years old (0) or 25 and under (1) */
    const PIPELINE_FACE_AGE = 'face_age';

    /** @var string passes the younger looking faces from 'face_age' to Azure for another age prediction */
    const PIPELINE_FACE_AZURE = 'face_azure';

    /** @var string detects actions in videos */
    const PIPELINE_ACTION = 'action';

    /** @var string detects location of video */
    const PIPELINE_LOCATION = 'location';

    /** @var string detects if any of strap-on, glasses, or cigarettes in video */
    const PIPELINE_OBJECT = 'object';

    /** @var string detects if video likely animated */
    const PIPELINE_HENTAI = 'hentai';

    /** @var string measures the quality of video frames */
    const PIPELINE_QUALITY = 'quality';

    /** @var string detects speech and classifies as female, male, trans */
    const PIPELINE_SPEECH = 'speech';

    /** @var string detects people in the video */
    const PIPELINE_PERSON = 'person';

    /** @var string counts the number of people in the video */
    const PIPELINE_ACTOR_COUNT = 'actor_count';

    /** @var string classifies clusters of detected faces by age range, gender, and race */
    const PIPELINE_FACE_CLASSIFY = 'face_classify';

    /** @var string predicts the genitals and sexuality of people detected in the videos */
    const PIPELINE_PERSON_CLASSIFY = 'person_classify';

    /**
     * @var string One of the ORIENTATION_* constants to help with action detection labels.
     */
    private $orientation = self::ORIENTATION_STRAIGHT;

    /**
     * @var string[] An array of PIPELINE_* constants, which name the pipelines through which to run the image/video.
     */
    private $pipelines = [];

    /**
     * @var FilePattern|null The location to which to store the face_azure snapshots, or null if they should not
     *      be stored.
     */
    private $azureFilePattern = null;

    /**
     * @return string One of the ORIENTATION_* constants to help with action detection labels.
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * @param string $orientation One of the ORIENTATION_* constants to help with action detection labels.
     * @return $this
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;
        return $this;
    }

    /**
     * @return \string[] An array of PIPELINE_* constants, which name the pipelines through which to run the
     *      image/video.
     */
    public function getPipelines()
    {
        return $this->pipelines;
    }

    /**
     * @param \string[] $pipelines An array of PIPELINE_* constants, which name the pipelines through which to run
     *      the image/video.
     * @return $this
     */
    public function setPipelines($pipelines)
    {
        $this->pipelines = $pipelines;
        return $this;
    }


    /**
     * @param FilePattern|null $azureFilePattern The location to which to store the face_azure snapshots, or null
     *      if they should not be stored.
     *
     * @return $this
     */
    final public function setAzureFilePattern(FilePattern $azureFilePattern = null)
    {
        $this->azureFilePattern = $azureFilePattern;

        return $this;
    }

    /**
     * @return FilePattern|null The location to which to store the face_azure snapshots, or null if they should
     *      not be stored.
     */
    final public function getAzureFilePattern()
    {
        return $this->azureFilePattern;
    }

}
