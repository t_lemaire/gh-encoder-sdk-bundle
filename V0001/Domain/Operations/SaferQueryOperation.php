<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;


abstract class SaferQueryOperation extends Operation implements GenerateCallBackInterface, HasNoFilePatternInterface
{
    /**
     * @var bool True iff a fake response should be generated that indicates a match.
     */
    private $forceMatchForTesting = false;

    /**
     * @return boolean True iff a fake response should be generated that indicates a match.
     */
    public function getForceMatchForTesting()
    {
        return $this->forceMatchForTesting;
    }

    /**
     * @param boolean $forceMatchForTesting True iff a fake response should be generated that indicates a match.
     * @return $this
     */
    public function setForceMatchForTesting($forceMatchForTesting)
    {
        $this->forceMatchForTesting = $forceMatchForTesting;
        return $this;
    }


}
