<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations;

use \GorillaHub\SDKs\SDKBundle\V0001\Domain\OperationInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePatternInterface;

interface VideoOperationInterface extends OperationInterface, FilePatternInterface
{

	/**
	 * Sets the url value.
	 *
	 * @param string $url
	 * @return self
	 */
	public function setUrl($url);


	/**
	 * Returns the url value.
	 *
	 * @return string
	 */
	public function getUrl();

}