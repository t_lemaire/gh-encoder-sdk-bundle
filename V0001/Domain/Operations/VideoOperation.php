<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations;


use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;

abstract class VideoOperation extends Operation implements VideoOperationInterface
{

	/**
	 * Where the completed video can be found.
	 *
	 * @var string
	 */
	private $url;

	/**
	 * @var FilePattern
	 */
	private $filePattern;

	/**
	 * @inheritdoc
	 *
	 * @throws InvalidParameterException
	 */
	final public function setUrl($url)
	{
		if (!is_string($url)) {
			throw new InvalidParameterException('Url has to be a string value.');
		}

		if (mb_strpos($url, 'http://') === 0 || mb_strpos($url, 'https://') === 0) {
			$this->url = $url;
		} else {
			throw new InvalidParameterException('Invalid url format.');
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	final public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param FilePattern $filePattern
	 *
	 * @return $this
	 */
	final public function setFilePattern(FilePattern $filePattern)
	{
		$this->filePattern = $filePattern;

		return $this;
	}

	/**
	 * @return FilePattern
	 */
	final public function getFilePattern()
	{
		return $this->filePattern;
	}

	public function autoSelectVolumeName() {
		if ($this->getVolumeName() !== null) {
			return $this->getVolumeName();
		}
		if ($this->getFilePattern() !== null) {
			if ($this->getFilePattern()->getVolumeName() !== null) {
				return $this->getFilePattern()->getVolumeName();
			}
		}
		return 'default';
	}

}