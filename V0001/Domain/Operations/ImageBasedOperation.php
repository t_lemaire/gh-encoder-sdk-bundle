<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations;

use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\FilePattern\FilePattern;
use \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\ImageMetrics;

abstract class ImageBasedOperation extends Operation
{

	/**
	 * @var array
	 */
	private $metrics = array();

	/**
	 * @param ImageMetrics $metric
	 *
	 * @throws \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @return $this
	 */
	final public function addMetric(ImageMetrics $metric)
	{
		if ($metric->getMetricId() == '') {
			$metric->setMetricId(uniqid());
		}

		if (null == $metric->getFilePattern()) {
			throw new InvalidParameterException('Invalid metric content, File Pattern definition is missing.');
		}

		if ($this->filePatternExists($metric->getFilePattern()) === true) {
			throw new InvalidParameterException('The file pattern definition has to be unique.');
		}

		$this->metrics[$metric->getMetricId()] = $metric;

		return $this;
	}

	/**
	 * @param ImageMetrics[] $metrics
	 *
	 * @throws \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException
	 * @return $this
	 */
	final public function setMetrics(array $metrics = array())
	{
		$this->metrics = array();

		foreach ($metrics as $metric) {
			if ($metric instanceof \GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Video\Encode\ImageMetrics) {

				$this->addMetric($metric);
			}
		}

		return $this;
	}

	/**
	 * @return ImageMetrics[]
	 */
	final public function getMetrics()
	{
		return $this->metrics;
	}

    /**
     * @param string $metricId
     * @return ImageMetrics|null The image metrics, or null if there are none with that ID.
     */
	final public function getMetric($metricId)
    {
	    return isset($this->metrics[$metricId]) ? $this->metrics[$metricId] : null;
    }

    /**
     * @param string $metricId
     * @return bool True if the image metric exists in this
     */
    final public function hasMetric($metricId)
    {
        return isset($this->metrics[$metricId]);
    }

	public function autoSelectVolumeName() {
		if ($this->getVolumeName() !== null) {
			return $this->getVolumeName();
		}
		foreach ($this->getMetrics() as $metrics) {
			if ($metrics->getFilePattern() !== null) {
				if ($metrics->getFilePattern()->getVolumeName() !== null) {
					return $metrics->getFilePattern()->getVolumeName();
				}
			}
		}
		return 'default';
	}

	/**
	 * Verify if the passed fieldPattern match any existing Metric file pattern
	 *
	 * @param FilePattern $filePattern
	 *
	 * @return bool
	 */
	private function filePatternExists(FilePattern $filePattern)
	{
		$filePatternString = (string) $filePattern;

		foreach ($this->getMetrics() as $metric) {
			if ($filePatternString === (string) $metric->getFilePattern()) {
				return true;
			}
		}

		return false;
	}

}