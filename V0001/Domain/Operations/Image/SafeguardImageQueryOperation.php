<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Image;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;


/**
 * If this operation is present in a job, then Safeguard (an internal MindGeek service) is queried to see if the
 * image is already in their database.  If this process is successful then the client receives a SuccessCall
 * callback with the "result" field set to an instance of SafeguardImageQueryResult.  Otherwise, the client receives a
 * FailureCall callback for this operation.
 *
 * While this operation is provided for convenience, it is recommended that you use SubscribeToSafeguardImageOperation
 * instead.  If you use that operation, then if your content is added to the Safeguard database at a later time
 * you will be notified.  See the docblock of SubscribeToSafeguardImageOperation for details.
 */
class SafeguardImageQueryOperation extends Operation implements GenerateCallBackInterface, HasNoFilePatternInterface
{

    /**
     * @var int|null The value below which the query will return matches for the p-hash metric, or null to use the
     *      server's default.  At the time of this writing, the server default value is 4.
     */
    private $matchThreshold = null;

    /**
     * @var float|null The value below which the query will return matches if the face distances match, or null
     *      to use the server's default.  At the time of this writing, the server default value is 0.2.
     */
    private $faceThreshold = null;

    /**
     * @var float|null The value below which the query will return matches if the DPH distances match, or null to
     *      use the server's default.  At the time of this writing, the server default value is 0.7.
     */
    private $dphThreshold = null;

    /**
     * @var string The name of the Safeguard database to check.
     */
    private $source = 'tubes';


    /**
     * @return int|null The value below which the query will return matches for the p-hash metric, or null to use the
     *      server's default.  At the time of this writing, the server default value is 4.
     */
    public function getMatchThreshold()
    {
        return $this->matchThreshold;
    }

    /**
     * @param int|null $matchThreshold The value below which the query will return matches for the p-hash metric, or
     *      null to use the server's default.  At the time of this writing, the server default value is 4.
     * @return $this
     */
    public function setMatchThreshold($matchThreshold)
    {
        $this->matchThreshold = $matchThreshold;
        return $this;
    }

    /**
     * @return float|null The value below which the query will return matches if the face distances match, or null
     *      to use the server's default.  At the time of this writing, the server default value is 0.2.
     */
    public function getFaceThreshold()
    {
        return $this->faceThreshold;
    }

    /**
     * @param float|null $faceThreshold The value below which the query will return matches if the face distances
     *      match, or null to use the server's default.  At the time of this writing, the server default value is 0.2.
     * @return $this
     */
    public function setFaceThreshold($faceThreshold)
    {
        $this->faceThreshold = $faceThreshold;
        return $this;
    }

    /**
     * @return float|null The value below which the query will return matches if the DPH distances match, or null to
     *      use the server's default.  At the time of this writing, the server default value is 0.7.
     */
    public function getDphThreshold()
    {
        return $this->dphThreshold;
    }

    /**
     * @param float|null $dphThreshold The value below which the query will return matches if the DPH distances match,
     *      or null to use the server's default.  At the time of this writing, the server default value is 0.7.
     * @return $this
     */
    public function setDphThreshold($dphThreshold)
    {
        $this->dphThreshold = $dphThreshold;
        return $this;
    }

    /**
     * @return string The name of the Safeguard database to check.
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source The name of the Safeguard database to check.
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }



}
