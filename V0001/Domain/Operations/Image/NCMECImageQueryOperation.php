<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Image;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\NCMECQueryOperation;


/**
 * If this operation is present in a job, then NCMEC (an external service) is queried to see if the image is
 * in their database.  If this process is successful then the client receives a SuccessCall callback with the "result"
 * field set to an instance of NCMECImageQueryResult.  Otherwise, the client receives a FailureCall callback for this
 * operation.
 */
class NCMECImageQueryOperation extends NCMECQueryOperation
{

}
