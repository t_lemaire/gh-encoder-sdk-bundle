<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Image;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;

/**
 * If this operation is present in a job, then Google Content Safety API is queried to see if the
 * image is likely to contain CSA material.  If this process is successful then the client receives a SuccessCall
 * callback with the "result" field set to an instance of GoogleContentSafetyQueryResult. Otherwise, the client
 * receives a FailureCall callback for this operation.
 */
class GoogleContentSafetyQueryOperation extends Operation implements GenerateCallBackInterface, HasNoFilePatternInterface
{

}


