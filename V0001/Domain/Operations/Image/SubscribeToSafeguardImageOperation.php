<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Image;


/**
 * This operation is like SafeguardImageQueryOperation, except for the following differences:
 *
 *      1) The usual SuccessCall will not contain a SafeguardImageQueryResult; it will contain a
 *          SubscribeToSafeguardImageQueryResult instead.  This class (deliberately) does not contain any information;
 *          the callback simply tells you that you are subscribed to updates.
 *
 *      2) The result is sent to your site's subscription callback URL (which is configured in the upload service
 *          database, in the `litmus`.`clients` table).  In particular, a SafeguardImageQueryUpdateCall is sent to
 *          the subscription URL.  Note that this call is different from the usual SafeguardImageQueryResult in that
 *          information about the original call and the original operation are not included.  If you want to identify
 *          which video the callback relates to, you must use the getCustomId() method of the
 *          SafeguardImageQueryUpdateCall.  This will return the custom ID that you specified by calling
 *          setCustomId() on the SubscribeToSafeguardImageOperation.
 *
 *      3) In addition to the first result, which is sent right away, whenever the result of the query changes in
 *          the future (someone registers a matching video to Safeguard), you will receive another
 *          SafeguardImageQueryUpdateCall to your subscription URL.
 */
class SubscribeToSafeguardImageOperation extends SafeguardImageQueryOperation
{
    /** @var string|null @see setSubscriptionCallbackUrl() */
    private $subscriptionCallbackUrl = null;

    /** @var int|null @see setExpiryTime() */
    private $expiryTime = null;


    /**
     * @return string @see setSubscriptionCallbackUrl()
     */
    public function getSubscriptionCallbackUrl()
    {
        return $this->subscriptionCallbackUrl;
    }

    /**
     * @param string|null $subscriptionCallbackUrl If this is not null, then any future SafeguardImageQueryUpdateCall
     *      calls are sent to this URL.  If this is null, then the site's default subscription callback URL is used.
     *      It is recommended that this is usually set to null, first to save space in the upload service database,
     *      and second so that it can be changed easily in the future if necessary.
     *      IMPORTANT:  This does NOT change the callback URL of the job, or this operation.  The
     *      SubscribeToSafeguardImageResult is sent to the usual callback URL that is specified for the overall job.
     *      This only affects where the SafeguardImageQueryUpdateCall is sent, and only for this particular image.
     *
     * @return $this
     */
    public function setSubscriptionCallbackUrl($subscriptionCallbackUrl)
    {
        $this->subscriptionCallbackUrl = $subscriptionCallbackUrl;
        return $this;
    }

    /**
     * @return int|null @see setExpiryTime()
     */
    public function getExpiryTime()
    {
        return $this->expiryTime;
    }

    /**
     * @param int|null $expiryTime The unix timestamp of the time at which this subscription should be deleted, or
     *      null if the subscription should never be deleted.  This feature is typically used for testing.
     *      Production subscriptions should not expire.
     * @return $this
     */
    public function setExpiryTime($expiryTime)
    {
        $this->expiryTime = $expiryTime;
        return $this;
    }


}
