<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Image;


/**
 * This operation is like PhotoDNAQueryOperation, except for the following differences:
 *
 *      1) The usual SuccessCall will not contain a PhotoDNAQueryResult; it will contain a
 *          SubscribeToPhotoDNAQueryResult instead.  This class (deliberately) does not contain any information;
 *          the callback simply tells you that you are subscribed to updates.
 *
 *      2) The result is sent to your site's subscription callback URL (which is configured in the upload service
 *          database, in the `litmus`.`clients` table).  In particular, a PhotoDNAQueryUpdateCall is sent to
 *          the subscription URL.  Note that this call is different from the usual PhotoDNAQueryResult in that
 *          information about the original call and the original operation are not included.  If you want to identify
 *          which video the callback relates to, you must use the getCustomId() method of the
 *          PhotoDNAQueryUpdateCall.  This will return the custom ID that you specified by calling
 *          setCustomId() on the SubscribeToPhotoDNAOperation.
 *
 *      3) In addition to the first result, which is sent right away, whenever the result of the query changes in
 *          the future (someone registers a matching image to PhotoDNA), you will receive another
 *          PhotoDNAQueryUpdateCall to your subscription URL; but since PhotoDNA doesn't support this feature, this
 *          call will not happen until the next scheduled rescan of your photo.
 */
class SubscribeToPhotoDNAOperation extends PhotoDNAQueryOperation
{
    /** @var string|null @see setSubscriptionCallbackUrl() */
    private $subscriptionCallbackUrl = null;

    /** @var int|null @see setExpiryTime() */
    private $expiryTime = null;

    /**
     * @var int|null The unix timestamp of the time at which the photo was first uploaded to the site, or the time at
     *      which it was created, or null if not known.  This is useful if $subscribe is true; if the file is old,
     *      we do not re-scan as frequently.
     */
    private $mediaTime;

    /** @var int|null @see setLastScanTime() */
    private $lastScanTime = null;

    /**
     * @return string @see setSubscriptionCallbackUrl()
     */
    public function getSubscriptionCallbackUrl()
    {
        return $this->subscriptionCallbackUrl;
    }

    /**
     * @param string|null $subscriptionCallbackUrl If this is not null, then any future PhotoDNAQueryUpdateCall
     *      calls are sent to this URL.  If this is null, then the site's default subscription callback URL is used.
     *      It is recommended that this is usually set to null, first to save space in the upload service database,
     *      and second so that it can be changed easily in the future if necessary.
     *      IMPORTANT:  This does NOT change the callback URL of the job, or this operation.  The
     *      SubscribeToPhotoDNAResult is sent to the usual callback URL that is specified for the overall job.
     *      This only affects where the PhotoDNAQueryUpdateCall is sent, and only for this particular image.
     *
     * @return $this
     */
    public function setSubscriptionCallbackUrl($subscriptionCallbackUrl)
    {
        $this->subscriptionCallbackUrl = $subscriptionCallbackUrl;
        return $this;
    }

    /**
     * @return int|null @see setExpiryTime()
     */
    public function getExpiryTime()
    {
        return $this->expiryTime;
    }

    /**
     * @param int|null $expiryTime The unix timestamp of the time at which this subscription should be deleted, or
     *      null if the subscription should never be deleted.  This feature is typically used for testing.
     *      Production subscriptions should not expire.
     * @return $this
     */
    public function setExpiryTime($expiryTime)
    {
        $this->expiryTime = $expiryTime;
        return $this;
    }


    /**
     * @return int|null  @see $mediaTime
     */
    public function getMediaTime()
    {
        return $this->mediaTime;
    }

    /**
     * @param int|null $mediaTime @see $mediaTime
     * @return $this
     */
    public function setMediaTime($mediaTime)
    {
        $this->mediaTime = $mediaTime;
        return $this;
    }

    /**
     * @return int|null  @see setLastScanTime()
     */
    public function getLastScanTime()
    {
        return $this->lastScanTime;
    }

    /**
     * @param int|null $lastScanTime If the client already has a result from PhotoDNA, then set this to the time
     *      of that result.  If this is not known, it should at least be estimated.  This time used to decide when
     *      the next re-scan is necessary.  If this is null, the photo is scanned immediately.
     * @return $this
     */
    public function setLastScanTime($lastScanTime)
    {
        $this->lastScanTime = $lastScanTime;
        return $this;
    }
}
