<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations\Image;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;


/**
 * If this operation is present in a job, then PhotoDNA (a Microsoft service) is queried to see if the
 * image is already in their database.  If this process is successful then the client receives a SuccessCall
 * callback with the "result" field set to an instance of PhotoDNAQueryResult.  Otherwise, the client receives a
 * FailureCall callback for this operation.
 *
 * While this operation is provided for convenience, it is recommended that you use SubscribeToPhotoDNAOperation
 * instead.  If you use that operation, then if your content is added to the PhotoDNA database at a later time
 * you will be notified.  See the docblock of SubscribeToPhotoDNAOperation for details.
 */
class PhotoDNAQueryOperation extends Operation implements GenerateCallBackInterface, HasNoFilePatternInterface
{


}
