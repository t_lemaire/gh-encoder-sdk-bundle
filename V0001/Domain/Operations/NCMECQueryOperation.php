<?php
namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operations;

use GorillaHub\SDKs\EncodeBundle\V0001\Domain\Operation;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\GenerateCallBackInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Operations\Descriptors\HasNoFilePatternInterface;


abstract class NCMECQueryOperation extends Operation implements GenerateCallBackInterface, HasNoFilePatternInterface
{
    /** @var bool */
    private $forceMatchForTesting = false;

    /**
     * @return boolean  @see setForceMatchForTesting()
     */
    public function getForceMatchForTesting()
    {
        return $this->forceMatchForTesting;
    }

    /**
     * @param boolean $forceMatchForTesting If this is true, then the operation will always match.  This can be used
     * 		for testing.
     * @return $this
     */
    public function setForceMatchForTesting($forceMatchForTesting)
    {
        $this->forceMatchForTesting = $forceMatchForTesting;
        return $this;
    }

}
