<?php

namespace GorillaHub\SDKs\EncodeBundle\V0001\Domain;


use \GorillaHub\SDKs\SDKBundle\V0001\Domain\SDKCallInterface;
use \GorillaHub\SDKs\SDKBundle\V0001\Domain\Signature;
use \GorillaHub\SDKs\SDKBundle\V0001\Exceptions\InvalidParameterException;

class CleanupRequest implements SDKCallInterface
{

    const STATUS_QUEUED = 'queued';
    const STATUS_PROCESSING = 'processing';
    const STATUS_DONE = 'done';


    /**
     * @var Signature
     */
    protected $signature;


    /**
     * @var int
     *
     */
    protected $operationId;

    /**
     * Sets the signature.
     *
     * @param Signature $signature
     *
     * @return self
     */
    final public function setSignature(Signature $signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Returns the signature.
     *
     * @return Signature
     */
    final public function getSignature()
    {
        return $this->signature;
    }


    /**
     * @inheritdoc
     */
    final public function setStatus($status)
    {
        if (!is_string($status)) {
            throw new InvalidParameterException('Status has to be a string value.');
        }

        $this->status = $status;
        return $this;

    }

    /**
     * @param int $operationId
     * @return self
     */
    public function setOperationId($operationId)
    {
        $this->operationId = $operationId;
        return $this;
    }

    /**
     * @return integer
     */
    public function getOperationId()
    {
        return $this->operationId;
    }


} 